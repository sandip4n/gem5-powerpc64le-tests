#!/usr/bin/python

# Filename: test-checker.py
# Authored-By: Sandipan Das
# Python-Version: 2.7

import re
import sys
import os.path
import subprocess

# Class to hold instruction execution details
class inst(object):
    # Constructor
    def __init__(self, addr, mnem, oprs, regs):
        self.addr = addr
        self.mnem = mnem
        self.oprs = oprs
        self.regs = regs

# Process output from qemu
def process_qemu(f, p):
    # Run program
    s = subprocess.Popen(['./ppc64le-linux-user/qemu-ppc64le',
                          '-cpu', 'POWER9', '-singlestep',
                          '-d', 'in_asm,cpu,nochain', os.path.abspath(f)],
                         cwd = os.path.abspath(p),
                         stdout = subprocess.PIPE, stderr = subprocess.PIPE)

    # Read raw data
    _, d = s.communicate()

    i = list()
    p = re.compile('IN:\s+\n+0x([\da-f]+):\s+((?:\w+[\.\+\-]?)|(?:\.long))(?'
                   ':(?:\n+)|(?:\s+((?:(?:(?:cr|[r-])?\d+|lt|gt|eq|so)\,?(?:'
                   '\s*)|(?:\(r\d+\))|(?:(?:0x)[\da-f]+)|(?:(?:\d+\*cr\d\+)?'
                   '(?:lt|gt|eq|so)))*)\n+))NIP\s+([\da-f]+)\s+LR\s+([\da-f]'
                   '+)\s+CTR\s+([\da-f]+)\s+XER\s+([\da-f]+)\s+CPU#\d+\n+MSR'
                   '\s+([\da-f]+)\s+HID0\s+[\da-f]+\s+HF\s+[\da-f]+\s+iidx\s'
                   '+\d+\s+didx\s+\d+\n+TB\s+[\da-f]+\s+\d+\n+GPR00\s+([\da-'
                   'f]+)\s+([\da-f]+)\s+([\da-f]+)\s+([\da-f]+)\n+GPR04\s+(['
                   '\da-f]+)\s+([\da-f]+)\s+([\da-f]+)\s+([0-9a-f]+)\n+GPR08'
                   '\s+([\da-f]+)\s+([\da-f]+)\s+([\da-f]+)\s+([\da-f]+)\n+G'
                   'PR12\s+([\da-f]+)\s+([\da-f]+)\s+([\da-f]+)\s+([\da-f]+)'
                   '\n+GPR16\s+([\da-f]+)\s+([\da-f]+)\s+([\da-f]+)\s+([\da-'
                   'f]+)\n+GPR20\s+([\da-f]+)\s+([\da-f]+)\s+([0-9a-f]+)\s+('
                   '[\da-f]+)\n+GPR24\s+([\da-f]+)\s+([\da-f]+)\s+([\da-f]+)'
                   '\s+([\da-f]+)\n+GPR28\s+([\da-f]+)\s+([\da-f]+)\s+([\da-'
                   'f]+)\s+([\da-f]+)\n+CR\s+([\da-f]+)\s+\[.*\]\s+RES\s+(?:'
                   '[\da-f]+)\n+FPR00\s+([\da-f]+)\s+([\da-f]+)\s+([\da-f]+)'
                   '\s+([\da-f]+)\n+FPR04\s+([\da-f]+)\s+([\da-f]+)\s+([\da-'
                   'f]+)\s+([\da-f]+)\n+FPR08\s+([\da-f]+)\s+([\da-f]+)\s+(['
                   '\da-f]+)\s+([\da-f]+)\n+FPR12\s+([\da-f]+)\s+([0-9a-f]+)'
                   '\s+([\da-f]+)[\s]+([\da-f]+)\n+FPR16\s+([\da-f]+)\s+([\d'
                   'a-f]+)\s+([\da-f]+)\s+([\da-f]+)\n+FPR20\s+([\da-f]+)\s+'
                   '([\da-f]+)\s+([\da-f]+)\s+([\da-f]+)\n+FPR24\s+([\da-f]+'
                   ')\s+([\da-f]+)\s+([\da-f]+)\s+([\da-f]+)\n+FPR28\s+([\da'
                   '-f]+)\s+([\da-f]+)\s+([\da-f]+)\s+([\da-f]+)\n+FPSCR\s+('
                   '[\da-f]+)\n+')

    for m in p.finditer(d):
        addr = int(m.group(1), 16)
        mnem = str(m.group(2))
        oprs = list()
        regs = dict()

        # Check if the instruction has any operands
        if m.group(3):
            # Extract register operands and immediates
            oprs = re.split('\(|\)|\s*\,?', m.group(3).strip())
            oprs = filter(bool, oprs)

        # Read special-purpose registers
        regs['NIA'  ] = int(m.group(4), 16)
        regs['LR'   ] = int(m.group(5), 16)
        regs['CTR'  ] = int(m.group(6), 16)
        regs['XER'  ] = int(m.group(7), 16)
        regs['MSR'  ] = int(m.group(8), 16)
        regs['CR'   ] = int(m.group(41), 16)
        regs['FPSCR'] = int(m.group(74), 16)

        # Read general-purpose and floating-point registers
        for ridx in range(32):
            regs['GPR' + str(ridx).zfill(2)] = int(m.group(9+ridx), 16)
            regs['FPR' + str(ridx).zfill(2)] = float.fromhex(m.group(42+ridx))

        # Add to list of instruction execution details
        i.append(inst(addr, mnem, oprs, regs))

    return i

# Process output from gem5
def process_gem5(f, p):
    # Run program
    s = subprocess.Popen(['./build/POWER/gem5.debug',
                          '--debug-flags=ExecAll,Registers',
                          'configs/example/se.py',
                          '--mem-size', '4294967296',
                          '--cmd', os.path.abspath(f)],
                         cwd = os.path.abspath(p),
                         stdout = subprocess.PIPE, stderr = subprocess.PIPE)

    # Read raw data
    d, _ = s.communicate()

    i = list()
    p = re.compile('\s*(\d+):\s+system\.cpu\s+A\d\s+T\d\s+:\s+\@([\w_][\w\d]'
                   '*\+?\d*)\s+:\s+(\w+\.?)((?:\s+)|(?:\s+(?:(?:(?:(?:cr|[r-'
                   '])?)\d+,?(?:\s*)|(?:\(r\d+\))|(?:(?:0x)[\da-f]+))*)\s+)|'
                   '(?:\s+[\w_][\w\d]*\+?\d*\s+)):')

    # Remember last instruction address
    last = str()

    for m in p.finditer(d):
        tick = int(m.group(1))
        addr = str(m.group(2))
        mnem = str(m.group(3))
        oprs = list()
        regs = dict()

        # If last instruction address is the same as the current address,
        # then it is the same instruction just requiring more ticks to
        # finish up. So, this can be skipped.
        if not last or last != addr:
            last = addr
            # Check if the instruction has any operands
            if m.group(4):
                # Extract register operands and immediates
                oprs = re.split('\(|\)|\s*\,?', m.group(4).strip())
                oprs = filter(bool, oprs)

            p = re.compile('\s+(?:' + str(tick) + '):\s+system\.cpu\.\[tid:['
                           '0-9]+\]:\s+Setting\s+int\s+reg\s+(\d+)\s+\((?:[0'
                           '-9]+)\)\s+to\s+(?:(?:0x)?([\da-f]+))')

            for m in p.finditer(d):
                ridx = int(m.group(1))
                rval = int(m.group(2), 16)

                # Read general-purpose registers
                if ridx < 32:
                    regs['GPR' + str(ridx).zfill(2)] = rval

                # Read special-purpose registers
                else:
                    if   ridx == 32: regs['CR'   ] = rval
                    elif ridx == 33: regs['XER'  ] = rval
                    elif ridx == 34: regs['LR'   ] = rval
                    elif ridx == 35: regs['CTR'  ] = rval
                    elif ridx == 36: regs['FPSCR'] = rval

            # Add to list of instruction execution details
            i.append(inst(addr, mnem, oprs, regs))

    return i

def main():
    # Check if input file is specified
    if len(sys.argv) < 2:
        print 'Usage: %s <input-file>' % sys.argv[0]
        sys.exit(0)

    # Check if input file exists
    if not os.path.isfile(sys.argv[1]):
        print 'Unable to access %s' % sys.argv[1]
        sys.exit(1)

    # Setup program paths
    qpath = os.path.expandvars('$QEMU_PATH')
    gpath = os.path.expandvars('$GEM5_PATH')

    # Check if path to programs are available
    if not os.path.exists(qpath):
        print 'Unable to find qemu'
        print 'Please set a valid \'QEMU_PATH\' value'
        sys.exit(1)
    elif not os.path.exists(gpath):
        print 'Unable to find gem5'
        print 'Please set a valid \'GEM5_PATH\' value'
        sys.exit(1)

    # Process input
    qout = process_qemu(sys.argv[1], qpath)
    gout = process_gem5(sys.argv[1], gpath)
    qlen = len(qout)
    glen = len(gout)

    if qlen != glen:
        print 'Something went terribly wrong!'
        sys.exit(1)

    # Compare register values for each instruction
    for i in range(glen-1):
        if not gout[i].regs:
            print ('[0x%016x, %-16s] \033[1m%-10s\033[0m %-40s') % (
                    qout[i].addr, gout[i].addr,
                    gout[i].mnem, gout[i].oprs)
        else:
            for gn, gv in gout[i].regs.iteritems():
                qv = qout[i+1].regs[gn]
                # Check if register values do not match
                if (gv != qv):
                    print ('[0x%016x, %-16s] \033[1m%-10s\033[0m %-40s'
                           '\033[94m%-6s\033[0m'
                           '\033[91mExp: 0x%016x, Got: 0x%016x\033[0m') % (
                           qout[i].addr, gout[i].addr,
                           gout[i].mnem, gout[i].oprs, gn, qv, gv)
                else:
                    print ('[0x%016x, %-16s] \033[1m%-10s\033[0m %-40s'
                           '\033[94m%-6s\033[0m'
                           '\033[92mExp: 0x%016x, Got: 0x%016x\033[0m') % (
                           qout[i].addr, gout[i].addr,
                           gout[i].mnem, gout[i].oprs, gn, qv, gv)

if __name__ == '__main__':
    main()
