CC = powerpc64le-linux-gnu-gcc
CFLAGS = -Wall -mcpu=power9 -nostdlib

SRCS = $(wildcard src/*.s)
BINS = $(patsubst src/%, bin/%, $(patsubst %.s, %, $(SRCS)))
DIRS!= $(shell mkdir -p bin)

.PHONY: all clean

all: $(BINS)

bin/%: src/%.s
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -f bin/*

distclean:
	rm -rf bin

