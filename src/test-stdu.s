.machine    power8
.abiversion 2

.text
.global     _start

_start:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Run test cases
    bl      run_test

    # Call exit()
    li      0, 1  # Syscall number (sys_exit)
    li      3, 1  # First argument is exit code
    sc            # Call kernel

run_test:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Do some work
    lis     5, test_data@h
    ori     5, 5, test_data@l
    mr      6, 5
    addi    6, 6, 32

    ldu     12, 0(5)  # Positive displacements
    stdu    12, 0(6)
    ldu     12, 0(6)
    ldu     12, 8(5)
    stdu    12, 8(6)
    ldu     12, 0(6)
    ldu     12, 8(5)
    stdu    12, 8(6)
    ldu     12, 0(6)
    ldu     12, 8(5)
    stdu    12, 8(6)
    ldu     12, 0(6)

    addi    5, 5, 40
    addi    6, 6, 40
    ldu     12, -8(5) # Negative displacements
    stdu    12, -8(6)
    ldu     12, 0(6)
    ldu     12, -8(5)
    stdu    12, -8(6)
    ldu     12, 0(6)
    ldu     12, -8(5)
    stdu    12, -8(6)
    ldu     12, 0(6)
    ldu     12, -8(5)
    stdu    12, -8(6)
    ldu     12, 0(6)

    # Destroy stack frame
    addi    1, 1, 16
    mtlr    0

    # Return
    blr

.data
.align 3
test_data:
    .quad 0x1000000000000000
    .quad 0x2000000000000000
    .quad 0x4000000000000000
    .quad 0x8000000000000000
