.machine    power8
.abiversion 2

.text
.global     _start

_start:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Run test cases
    bl      run_test

    # Call exit()
    li      0, 1  # Syscall number (sys_exit)
    li      3, 1  # First argument is exit code
    sc            # Call kernel

run_test:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Do some work
    lis     5, test_data@h
    ori     5, 5, test_data@l
    lis     6, test_disp@h
    ori     6, 6, test_disp@l
    mr      7, 6
    addi    7, 7, 16
    li      9, 0

    subi    5, 5, 8
    subi    7, 7, 8
    ld      8, 0(6)  # Positive displacements
    ldux    12, 5, 8
    stdux   12, 7, 8
    ldux    12, 7, 9
    ldux    12, 5, 8
    stdux   12, 7, 8
    ldux    12, 7, 9
    ldux    12, 5, 8
    stdux   12, 7, 8
    ldux    12, 7, 9
    ldux    12, 5, 8
    stdux   12, 7, 8
    ldux    12, 7, 9

    addi    5, 5, 8
    addi    7, 7, 8
    ld      8, 8(6)  # Negative displacements
    ldux    12, 5, 8
    stdux   12, 7, 8
    ldux    12, 7, 9
    ldux    12, 5, 8
    stdux   12, 7, 8
    ldux    12, 7, 9
    ldux    12, 5, 8
    stdux   12, 7, 8
    ldux    12, 7, 9
    ldux    12, 5, 8
    stdux   12, 7, 8
    ldux    12, 7, 9

    # Destroy stack frame
    addi    1, 1, 16
    mtlr    0

    # Return
    blr

.data
.align 3
test_data:
    .quad 0x1000000000000000
    .quad 0x2000000000000000
    .quad 0x4000000000000000
    .quad 0x8000000000000000

test_disp:
    .quad 0x0000000000000008
    .quad 0xfffffffffffffff8
