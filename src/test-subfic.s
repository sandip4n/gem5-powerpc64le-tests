.machine    power8
.abiversion 2

.text
.global     _start

_start:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Run test cases
    bl      run_test

    # Call exit()
    li      0, 1  # Syscall number (sys_exit)
    li      3, 1  # First argument is exit code
    sc            # Call kernel

run_test:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Do some work
    lis     5, test_data@h
    ori     5, 5, test_data@l
    li      7, 0

    ld      12, 0(5)
    subfic  13, 12, -32768
    mfxer   6
    mtxer   7
    ldu     12, 8(5)
    subfic  13, 12, -32768
    mfxer   6
    mtxer   7
    ldu     12, 8(5)
    subfic  13, 12, -32768
    mfxer   6
    mtxer   7
    ldu     12, 8(5)
    subfic  13, 12, 32767
    mfxer   6
    mtxer   7
    ldu     12, 8(5)
    subfic  13, 12, 32767
    mfxer   6
    mtxer   7
    ldu     12, 8(5)
    subfic  13, 12, 32767
    mfxer   6
    mtxer   7

    # Destroy stack frame
    addi    1, 1, 16
    mtlr    0

    # Return
    blr

.data
.align 3
test_data:
    .quad 0x0000000000000000
    .quad 0x0000000100000000
    .quad 0x0000000000008000
    .quad 0xffffffffffff8001
    .quad 0xffffffff00007000
    .quad 0x0000000000007000
