.machine    power8
.abiversion 2

.text
.global     _start

_start:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Run test cases
    bl      run_test

    # Call exit()
    li      0, 1  # Syscall number (sys_exit)
    li      3, 1  # First argument is exit code
    sc            # Call kernel

run_test:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Do some work
    lis     5, test_data@h
    ori     5, 5, test_data@l
    lis     6, test_disp@h
    ori     6, 6, test_disp@l
    mr      7, 6
    addi    7, 7, 16
    li      9, 0

    subi    5, 5, 1
    subi    7, 7, 1
    ld      8, 0(6)  # Positive displacements
    lbzux   12, 5, 8
    stbux   12, 7, 8
    lbzux   12, 7, 9
    lbzux   12, 5, 8
    stbux   12, 7, 8
    lbzux   12, 7, 9
    lbzux   12, 5, 8
    stbux   12, 7, 8
    lbzux   12, 7, 9
    lbzux   12, 5, 8
    stbux   12, 7, 8
    lbzux   12, 7, 9

    addi    5, 5, 1
    addi    7, 7, 1
    ld      8, 8(6)  # Negative displacements
    lbzux   12, 5, 8
    stbux   12, 7, 8
    lbzux   12, 7, 9
    lbzux   12, 5, 8
    stbux   12, 7, 8
    lbzux   12, 7, 9
    lbzux   12, 5, 8
    stbux   12, 7, 8
    lbzux   12, 7, 9
    lbzux   12, 5, 8
    stbux   12, 7, 8
    lbzux   12, 7, 9

    # Destroy stack frame
    addi    1, 1, 16
    mtlr    0

    # Return
    blr

.data
.align 3
test_data:
    .byte 0x10
    .byte 0x20
    .byte 0x40
    .byte 0x80

test_disp:
    .quad 0x0000000000000001
    .quad 0xffffffffffffffff
