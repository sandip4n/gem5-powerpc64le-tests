.machine    power8
.abiversion 2

.text
.global     _start

_start:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Run test cases
    bl      run_test

    # Call exit()
    li      0, 1  # Syscall number (sys_exit)
    li      3, 1  # First argument is exit code
    sc            # Call kernel

run_test:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Do some work
    lis     5, test_data@h
    ori     5, 5, test_data@l
    lis     6, test_disp@h
    ori     6, 6, test_disp@l
    mr      7, 6
    addi    7, 7, 16

    # When RA = base address, RB = displacement, EA = (RA) + (RB)
    subi    5, 5, 8
    subi    7, 7, 8
    ld      8, 0(6)  # Positive displacements
    ldx     12, 5, 8
    stdx    12, 7, 8
    ldx     12, 7, 8
    addi    8, 8, 8
    ldx     12, 5, 8
    stdx    12, 7, 8
    ldx     12, 7, 8
    addi    8, 8, 8
    ldx     12, 5, 8
    stdx    12, 7, 8
    ldx     12, 7, 8
    addi    8, 8, 8
    ldx     12, 5, 8
    stdx    12, 7, 8
    ldx     12, 7, 8

    addi    5, 5, 40
    addi    7, 7, 40
    ld      8, 8(6)  # Negative displacements
    ldx     12, 5, 8
    stdx    12, 7, 8
    ldx     12, 7, 8
    subi    8, 8, 8
    ldx     12, 5, 8
    stdx    12, 7, 8
    ldx     12, 7, 8
    subi    8, 8, 8
    ldx     12, 5, 8
    stdx    12, 7, 8
    ldx     12, 7, 8
    subi    8, 8, 8
    ldx     12, 5, 8
    stdx    12, 7, 8
    ldx     12, 7, 8

    # When RA = displacement, RB = base address, EA = (RA) + (RB)
    subi    5, 5, 40
    subi    7, 7, 40
    ld      8, 0(6)  # Positive displacements
    ldx     12, 8, 5
    stdx    12, 8, 7
    ldx     12, 8, 7
    addi    8, 8, 8
    ldx     12, 8, 5
    stdx    12, 8, 7
    ldx     12, 8, 7
    addi    8, 8, 8
    ldx     12, 8, 5
    stdx    12, 8, 7
    ldx     12, 8, 7
    addi    8, 8, 8
    ldx     12, 8, 5
    stdx    12, 8, 7
    ldx     12, 8, 7

    addi    5, 5, 40
    addi    7, 7, 40
    ld      8, 8(6)  # Negative displacements
    ldx     12, 8, 5
    stdx    12, 8, 7
    ldx     12, 8, 7
    subi    8, 8, 8
    ldx     12, 8, 5
    stdx    12, 8, 7
    ldx     12, 8, 7
    subi    8, 8, 8
    ldx     12, 8, 5
    stdx    12, 8, 7
    ldx     12, 8, 7
    subi    8, 8, 8
    ldx     12, 8, 5
    stdx    12, 8, 7
    ldx     12, 8, 7

    # When RA = 0, EA = 0 + (RB)
    subi    5, 5, 32
    subi    7, 7, 32
    ldx     12, 0, 5
    stdx    12, 0, 7
    ldx     12, 0, 7
    addi    5, 5, 8
    addi    7, 7, 8
    ldx     12, 0, 5
    stdx    12, 0, 7
    ldx     12, 0, 7
    addi    5, 5, 8
    addi    7, 7, 8
    ldx     12, 0, 5
    stdx    12, 0, 7
    ldx     12, 0, 7
    addi    5, 5, 8
    addi    7, 7, 8
    ldx     12, 0, 5
    stdx    12, 0, 7
    ldx     12, 0, 7

    # Destroy stack frame
    addi    1, 1, 16
    mtlr    0

    # Return
    blr

.data
.align 3
test_data:
    .quad 0x1000000000000000
    .quad 0x2000000000000000
    .quad 0x4000000000000000
    .quad 0x8000000000000000

test_disp:
    .quad 0x0000000000000008
    .quad 0xfffffffffffffff8
