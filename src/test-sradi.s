.machine    power8
.abiversion 2

.text
.global     _start

_start:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Run test cases
    bl      run_test

    # Call exit()
    li      0, 1  # Syscall number (sys_exit)
    li      3, 1  # First argument is exit code
    sc            # Call kernel

run_test:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Do some work
    lis     5, test_data@h
    ori     5, 5, test_data@l
    li      8, 0

    # When Rc = 0
    mr      6, 5
    ld      12, 0(6)
    sradi   13, 12, 0
    mfxer   7
    mtxer   8
    sradi   13, 12, 1
    mfxer   7
    mtxer   8
    sradi   13, 12, 2
    mfxer   7
    mtxer   8
    sradi   13, 12, 3
    mfxer   7
    mtxer   8
    sradi   13, 12, 4
    mfxer   7
    mtxer   8
    sradi   13, 12, 7
    mfxer   7
    mtxer   8
    sradi   13, 12, 8
    mfxer   7
    mtxer   8
    sradi   13, 12, 15
    mfxer   7
    mtxer   8
    sradi   13, 12, 16
    mfxer   7
    mtxer   8
    sradi   13, 12, 31
    mfxer   7
    mtxer   8
    sradi   13, 12, 32
    mfxer   7
    mtxer   8
    sradi   13, 12, 63
    mfxer   7
    mtxer   8

    ldu     12, 8(6)
    sradi   13, 12, 0
    mfxer   7
    mtxer   8
    sradi   13, 12, 1
    mfxer   7
    mtxer   8
    sradi   13, 12, 2
    mfxer   7
    mtxer   8
    sradi   13, 12, 3
    mfxer   7
    mtxer   8
    sradi   13, 12, 4
    mfxer   7
    mtxer   8
    sradi   13, 12, 7
    mfxer   7
    mtxer   8
    sradi   13, 12, 8
    mfxer   7
    mtxer   8
    sradi   13, 12, 15
    mfxer   7
    mtxer   8
    sradi   13, 12, 16
    mfxer   7
    mtxer   8
    sradi   13, 12, 31
    mfxer   7
    mtxer   8
    sradi   13, 12, 32
    mfxer   7
    mtxer   8
    sradi   13, 12, 63
    mfxer   7
    mtxer   8

    ldu     12, 8(6)
    sradi   13, 12, 0
    mfxer   7
    mtxer   8
    sradi   13, 12, 1
    mfxer   7
    mtxer   8
    sradi   13, 12, 2
    mfxer   7
    mtxer   8
    sradi   13, 12, 3
    mfxer   7
    mtxer   8
    sradi   13, 12, 4
    mfxer   7
    mtxer   8
    sradi   13, 12, 7
    mfxer   7
    mtxer   8
    sradi   13, 12, 8
    mfxer   7
    mtxer   8
    sradi   13, 12, 15
    mfxer   7
    mtxer   8
    sradi   13, 12, 16
    mfxer   7
    mtxer   8
    sradi   13, 12, 31
    mfxer   7
    mtxer   8
    sradi   13, 12, 32
    mfxer   7
    mtxer   8
    sradi   13, 12, 63
    mfxer   7
    mtxer   8

    ldu     12, 8(6)
    sradi   13, 12, 0
    mfxer   7
    mtxer   8
    sradi   13, 12, 1
    mfxer   7
    mtxer   8
    sradi   13, 12, 2
    mfxer   7
    mtxer   8
    sradi   13, 12, 3
    mfxer   7
    mtxer   8
    sradi   13, 12, 4
    mfxer   7
    mtxer   8
    sradi   13, 12, 7
    mfxer   7
    mtxer   8
    sradi   13, 12, 8
    mfxer   7
    mtxer   8
    sradi   13, 12, 15
    mfxer   7
    mtxer   8
    sradi   13, 12, 16
    mfxer   7
    mtxer   8
    sradi   13, 12, 31
    mfxer   7
    mtxer   8
    sradi   13, 12, 32
    mfxer   7
    mtxer   8
    sradi   13, 12, 63
    mfxer   7
    mtxer   8

    ldu     12, 8(6)
    sradi   13, 12, 0
    mfxer   7
    mtxer   8
    sradi   13, 12, 1
    mfxer   7
    mtxer   8
    sradi   13, 12, 2
    mfxer   7
    mtxer   8
    sradi   13, 12, 3
    mfxer   7
    mtxer   8
    sradi   13, 12, 4
    mfxer   7
    mtxer   8
    sradi   13, 12, 7
    mfxer   7
    mtxer   8
    sradi   13, 12, 8
    mfxer   7
    mtxer   8
    sradi   13, 12, 15
    mfxer   7
    mtxer   8
    sradi   13, 12, 16
    mfxer   7
    mtxer   8
    sradi   13, 12, 31
    mfxer   7
    mtxer   8
    sradi   13, 12, 32
    mfxer   7
    mtxer   8
    sradi   13, 12, 63
    mfxer   7
    mtxer   8

    ldu     12, 8(6)
    sradi   13, 12, 0
    mfxer   7
    mtxer   8
    sradi   13, 12, 1
    mfxer   7
    mtxer   8
    sradi   13, 12, 2
    mfxer   7
    mtxer   8
    sradi   13, 12, 3
    mfxer   7
    mtxer   8
    sradi   13, 12, 4
    mfxer   7
    mtxer   8
    sradi   13, 12, 7
    mfxer   7
    mtxer   8
    sradi   13, 12, 8
    mfxer   7
    mtxer   8
    sradi   13, 12, 15
    mfxer   7
    mtxer   8
    sradi   13, 12, 16
    mfxer   7
    mtxer   8
    sradi   13, 12, 31
    mfxer   7
    mtxer   8
    sradi   13, 12, 32
    mfxer   7
    mtxer   8
    sradi   13, 12, 63
    mfxer   7
    mtxer   8

    ldu     12, 8(6)
    sradi   13, 12, 0
    mfxer   7
    mtxer   8
    sradi   13, 12, 1
    mfxer   7
    mtxer   8
    sradi   13, 12, 2
    mfxer   7
    mtxer   8
    sradi   13, 12, 3
    mfxer   7
    mtxer   8
    sradi   13, 12, 4
    mfxer   7
    mtxer   8
    sradi   13, 12, 7
    mfxer   7
    mtxer   8
    sradi   13, 12, 8
    mfxer   7
    mtxer   8
    sradi   13, 12, 15
    mfxer   7
    mtxer   8
    sradi   13, 12, 16
    mfxer   7
    mtxer   8
    sradi   13, 12, 31
    mfxer   7
    mtxer   8
    sradi   13, 12, 32
    mfxer   7
    mtxer   8
    sradi   13, 12, 63
    mfxer   7
    mtxer   8

    ldu     12, 8(6)
    sradi   13, 12, 0
    mfxer   7
    mtxer   8
    sradi   13, 12, 1
    mfxer   7
    mtxer   8
    sradi   13, 12, 2
    mfxer   7
    mtxer   8
    sradi   13, 12, 3
    mfxer   7
    mtxer   8
    sradi   13, 12, 4
    mfxer   7
    mtxer   8
    sradi   13, 12, 7
    mfxer   7
    mtxer   8
    sradi   13, 12, 8
    mfxer   7
    mtxer   8
    sradi   13, 12, 15
    mfxer   7
    mtxer   8
    sradi   13, 12, 16
    mfxer   7
    mtxer   8
    sradi   13, 12, 31
    mfxer   7
    mtxer   8
    sradi   13, 12, 32
    mfxer   7
    mtxer   8
    sradi   13, 12, 63
    mfxer   7
    mtxer   8

    ldu     12, 8(6)
    sradi   13, 12, 0
    mfxer   7
    mtxer   8
    sradi   13, 12, 1
    mfxer   7
    mtxer   8
    sradi   13, 12, 2
    mfxer   7
    mtxer   8
    sradi   13, 12, 3
    mfxer   7
    mtxer   8
    sradi   13, 12, 4
    mfxer   7
    mtxer   8
    sradi   13, 12, 7
    mfxer   7
    mtxer   8
    sradi   13, 12, 8
    mfxer   7
    mtxer   8
    sradi   13, 12, 15
    mfxer   7
    mtxer   8
    sradi   13, 12, 16
    mfxer   7
    mtxer   8
    sradi   13, 12, 31
    mfxer   7
    mtxer   8
    sradi   13, 12, 32
    mfxer   7
    mtxer   8
    sradi   13, 12, 63
    mfxer   7
    mtxer   8

    ldu     12, 8(6)
    sradi   13, 12, 0
    mfxer   7
    mtxer   8
    sradi   13, 12, 1
    mfxer   7
    mtxer   8
    sradi   13, 12, 2
    mfxer   7
    mtxer   8
    sradi   13, 12, 3
    mfxer   7
    mtxer   8
    sradi   13, 12, 4
    mfxer   7
    mtxer   8
    sradi   13, 12, 7
    mfxer   7
    mtxer   8
    sradi   13, 12, 8
    mfxer   7
    mtxer   8
    sradi   13, 12, 15
    mfxer   7
    mtxer   8
    sradi   13, 12, 16
    mfxer   7
    mtxer   8
    sradi   13, 12, 31
    mfxer   7
    mtxer   8
    sradi   13, 12, 32
    mfxer   7
    mtxer   8
    sradi   13, 12, 63
    mfxer   7
    mtxer   8

    ldu     12, 8(6)
    sradi   13, 12, 0
    mfxer   7
    mtxer   8
    sradi   13, 12, 1
    mfxer   7
    mtxer   8
    sradi   13, 12, 2
    mfxer   7
    mtxer   8
    sradi   13, 12, 3
    mfxer   7
    mtxer   8
    sradi   13, 12, 4
    mfxer   7
    mtxer   8
    sradi   13, 12, 7
    mfxer   7
    mtxer   8
    sradi   13, 12, 8
    mfxer   7
    mtxer   8
    sradi   13, 12, 15
    mfxer   7
    mtxer   8
    sradi   13, 12, 16
    mfxer   7
    mtxer   8
    sradi   13, 12, 31
    mfxer   7
    mtxer   8
    sradi   13, 12, 32
    mfxer   7
    mtxer   8
    sradi   13, 12, 63
    mfxer   7
    mtxer   8

    ldu     12, 8(6)
    sradi   13, 12, 0
    mfxer   7
    mtxer   8
    sradi   13, 12, 1
    mfxer   7
    mtxer   8
    sradi   13, 12, 2
    mfxer   7
    mtxer   8
    sradi   13, 12, 3
    mfxer   7
    mtxer   8
    sradi   13, 12, 4
    mfxer   7
    mtxer   8
    sradi   13, 12, 7
    mfxer   7
    mtxer   8
    sradi   13, 12, 8
    mfxer   7
    mtxer   8
    sradi   13, 12, 15
    mfxer   7
    mtxer   8
    sradi   13, 12, 16
    mfxer   7
    mtxer   8
    sradi   13, 12, 31
    mfxer   7
    mtxer   8
    sradi   13, 12, 32
    mfxer   7
    mtxer   8
    sradi   13, 12, 63
    mfxer   7
    mtxer   8

    ldu     12, 8(6)
    sradi   13, 12, 0
    mfxer   7
    mtxer   8
    sradi   13, 12, 1
    mfxer   7
    mtxer   8
    sradi   13, 12, 2
    mfxer   7
    mtxer   8
    sradi   13, 12, 3
    mfxer   7
    mtxer   8
    sradi   13, 12, 4
    mfxer   7
    mtxer   8
    sradi   13, 12, 7
    mfxer   7
    mtxer   8
    sradi   13, 12, 8
    mfxer   7
    mtxer   8
    sradi   13, 12, 15
    mfxer   7
    mtxer   8
    sradi   13, 12, 16
    mfxer   7
    mtxer   8
    sradi   13, 12, 31
    mfxer   7
    mtxer   8
    sradi   13, 12, 32
    mfxer   7
    mtxer   8
    sradi   13, 12, 63
    mfxer   7
    mtxer   8

    # When Rc = 1
    mr      6, 5
    ld      12, 0(6)
    sradi.  13, 12, 0
    mfxer   7
    mtxer   8
    sradi.  13, 12, 1
    mfxer   7
    mtxer   8
    sradi.  13, 12, 2
    mfxer   7
    mtxer   8
    sradi.  13, 12, 3
    mfxer   7
    mtxer   8
    sradi.  13, 12, 4
    mfxer   7
    mtxer   8
    sradi.  13, 12, 7
    mfxer   7
    mtxer   8
    sradi.  13, 12, 8
    mfxer   7
    mtxer   8
    sradi.  13, 12, 15
    mfxer   7
    mtxer   8
    sradi.  13, 12, 16
    mfxer   7
    mtxer   8
    sradi.  13, 12, 31
    mfxer   7
    mtxer   8
    sradi.  13, 12, 32
    mfxer   7
    mtxer   8
    sradi.  13, 12, 63
    mfxer   7
    mtxer   8

    ldu     12, 8(6)
    sradi.  13, 12, 0
    mfxer   7
    mtxer   8
    sradi.  13, 12, 1
    mfxer   7
    mtxer   8
    sradi.  13, 12, 2
    mfxer   7
    mtxer   8
    sradi.  13, 12, 3
    mfxer   7
    mtxer   8
    sradi.  13, 12, 4
    mfxer   7
    mtxer   8
    sradi.  13, 12, 7
    mfxer   7
    mtxer   8
    sradi.  13, 12, 8
    mfxer   7
    mtxer   8
    sradi.  13, 12, 15
    mfxer   7
    mtxer   8
    sradi.  13, 12, 16
    mfxer   7
    mtxer   8
    sradi.  13, 12, 31
    mfxer   7
    mtxer   8
    sradi.  13, 12, 32
    mfxer   7
    mtxer   8
    sradi.  13, 12, 63
    mfxer   7
    mtxer   8

    ldu     12, 8(6)
    sradi.  13, 12, 0
    mfxer   7
    mtxer   8
    sradi.  13, 12, 1
    mfxer   7
    mtxer   8
    sradi.  13, 12, 2
    mfxer   7
    mtxer   8
    sradi.  13, 12, 3
    mfxer   7
    mtxer   8
    sradi.  13, 12, 4
    mfxer   7
    mtxer   8
    sradi.  13, 12, 7
    mfxer   7
    mtxer   8
    sradi.  13, 12, 8
    mfxer   7
    mtxer   8
    sradi.  13, 12, 15
    mfxer   7
    mtxer   8
    sradi.  13, 12, 16
    mfxer   7
    mtxer   8
    sradi.  13, 12, 31
    mfxer   7
    mtxer   8
    sradi.  13, 12, 32
    mfxer   7
    mtxer   8
    sradi.  13, 12, 63
    mfxer   7
    mtxer   8

    ldu     12, 8(6)
    sradi.  13, 12, 0
    mfxer   7
    mtxer   8
    sradi.  13, 12, 1
    mfxer   7
    mtxer   8
    sradi.  13, 12, 2
    mfxer   7
    mtxer   8
    sradi.  13, 12, 3
    mfxer   7
    mtxer   8
    sradi.  13, 12, 4
    mfxer   7
    mtxer   8
    sradi.  13, 12, 7
    mfxer   7
    mtxer   8
    sradi.  13, 12, 8
    mfxer   7
    mtxer   8
    sradi.  13, 12, 15
    mfxer   7
    mtxer   8
    sradi.  13, 12, 16
    mfxer   7
    mtxer   8
    sradi.  13, 12, 31
    mfxer   7
    mtxer   8
    sradi.  13, 12, 32
    mfxer   7
    mtxer   8
    sradi.  13, 12, 63
    mfxer   7
    mtxer   8

    ldu     12, 8(6)
    sradi.  13, 12, 0
    mfxer   7
    mtxer   8
    sradi.  13, 12, 1
    mfxer   7
    mtxer   8
    sradi.  13, 12, 2
    mfxer   7
    mtxer   8
    sradi.  13, 12, 3
    mfxer   7
    mtxer   8
    sradi.  13, 12, 4
    mfxer   7
    mtxer   8
    sradi.  13, 12, 7
    mfxer   7
    mtxer   8
    sradi.  13, 12, 8
    mfxer   7
    mtxer   8
    sradi.  13, 12, 15
    mfxer   7
    mtxer   8
    sradi.  13, 12, 16
    mfxer   7
    mtxer   8
    sradi.  13, 12, 31
    mfxer   7
    mtxer   8
    sradi.  13, 12, 32
    mfxer   7
    mtxer   8
    sradi.  13, 12, 63
    mfxer   7
    mtxer   8

    ldu     12, 8(6)
    sradi.  13, 12, 0
    mfxer   7
    mtxer   8
    sradi.  13, 12, 1
    mfxer   7
    mtxer   8
    sradi.  13, 12, 2
    mfxer   7
    mtxer   8
    sradi.  13, 12, 3
    mfxer   7
    mtxer   8
    sradi.  13, 12, 4
    mfxer   7
    mtxer   8
    sradi.  13, 12, 7
    mfxer   7
    mtxer   8
    sradi.  13, 12, 8
    mfxer   7
    mtxer   8
    sradi.  13, 12, 15
    mfxer   7
    mtxer   8
    sradi.  13, 12, 16
    mfxer   7
    mtxer   8
    sradi.  13, 12, 31
    mfxer   7
    mtxer   8
    sradi.  13, 12, 32
    mfxer   7
    mtxer   8
    sradi.  13, 12, 63
    mfxer   7
    mtxer   8

    ldu     12, 8(6)
    sradi.  13, 12, 0
    mfxer   7
    mtxer   8
    sradi.  13, 12, 1
    mfxer   7
    mtxer   8
    sradi.  13, 12, 2
    mfxer   7
    mtxer   8
    sradi.  13, 12, 3
    mfxer   7
    mtxer   8
    sradi.  13, 12, 4
    mfxer   7
    mtxer   8
    sradi.  13, 12, 7
    mfxer   7
    mtxer   8
    sradi.  13, 12, 8
    mfxer   7
    mtxer   8
    sradi.  13, 12, 15
    mfxer   7
    mtxer   8
    sradi.  13, 12, 16
    mfxer   7
    mtxer   8
    sradi.  13, 12, 31
    mfxer   7
    mtxer   8
    sradi.  13, 12, 32
    mfxer   7
    mtxer   8
    sradi.  13, 12, 63
    mfxer   7
    mtxer   8

    ldu     12, 8(6)
    sradi.  13, 12, 0
    mfxer   7
    mtxer   8
    sradi.  13, 12, 1
    mfxer   7
    mtxer   8
    sradi.  13, 12, 2
    mfxer   7
    mtxer   8
    sradi.  13, 12, 3
    mfxer   7
    mtxer   8
    sradi.  13, 12, 4
    mfxer   7
    mtxer   8
    sradi.  13, 12, 7
    mfxer   7
    mtxer   8
    sradi.  13, 12, 8
    mfxer   7
    mtxer   8
    sradi.  13, 12, 15
    mfxer   7
    mtxer   8
    sradi.  13, 12, 16
    mfxer   7
    mtxer   8
    sradi.  13, 12, 31
    mfxer   7
    mtxer   8
    sradi.  13, 12, 32
    mfxer   7
    mtxer   8
    sradi.  13, 12, 63
    mfxer   7
    mtxer   8

    ldu     12, 8(6)
    sradi.  13, 12, 0
    mfxer   7
    mtxer   8
    sradi.  13, 12, 1
    mfxer   7
    mtxer   8
    sradi.  13, 12, 2
    mfxer   7
    mtxer   8
    sradi.  13, 12, 3
    mfxer   7
    mtxer   8
    sradi.  13, 12, 4
    mfxer   7
    mtxer   8
    sradi.  13, 12, 7
    mfxer   7
    mtxer   8
    sradi.  13, 12, 8
    mfxer   7
    mtxer   8
    sradi.  13, 12, 15
    mfxer   7
    mtxer   8
    sradi.  13, 12, 16
    mfxer   7
    mtxer   8
    sradi.  13, 12, 31
    mfxer   7
    mtxer   8
    sradi.  13, 12, 32
    mfxer   7
    mtxer   8
    sradi.  13, 12, 63
    mfxer   7
    mtxer   8

    ldu     12, 8(6)
    sradi.  13, 12, 0
    mfxer   7
    mtxer   8
    sradi.  13, 12, 1
    mfxer   7
    mtxer   8
    sradi.  13, 12, 2
    mfxer   7
    mtxer   8
    sradi.  13, 12, 3
    mfxer   7
    mtxer   8
    sradi.  13, 12, 4
    mfxer   7
    mtxer   8
    sradi.  13, 12, 7
    mfxer   7
    mtxer   8
    sradi.  13, 12, 8
    mfxer   7
    mtxer   8
    sradi.  13, 12, 15
    mfxer   7
    mtxer   8
    sradi.  13, 12, 16
    mfxer   7
    mtxer   8
    sradi.  13, 12, 31
    mfxer   7
    mtxer   8
    sradi.  13, 12, 32
    mfxer   7
    mtxer   8
    sradi.  13, 12, 63
    mfxer   7
    mtxer   8

    ldu     12, 8(6)
    sradi.  13, 12, 0
    mfxer   7
    mtxer   8
    sradi.  13, 12, 1
    mfxer   7
    mtxer   8
    sradi.  13, 12, 2
    mfxer   7
    mtxer   8
    sradi.  13, 12, 3
    mfxer   7
    mtxer   8
    sradi.  13, 12, 4
    mfxer   7
    mtxer   8
    sradi.  13, 12, 7
    mfxer   7
    mtxer   8
    sradi.  13, 12, 8
    mfxer   7
    mtxer   8
    sradi.  13, 12, 15
    mfxer   7
    mtxer   8
    sradi.  13, 12, 16
    mfxer   7
    mtxer   8
    sradi.  13, 12, 31
    mfxer   7
    mtxer   8
    sradi.  13, 12, 32
    mfxer   7
    mtxer   8
    sradi.  13, 12, 63
    mfxer   7
    mtxer   8

    ldu     12, 8(6)
    sradi.  13, 12, 0
    mfxer   7
    mtxer   8
    sradi.  13, 12, 1
    mfxer   7
    mtxer   8
    sradi.  13, 12, 2
    mfxer   7
    mtxer   8
    sradi.  13, 12, 3
    mfxer   7
    mtxer   8
    sradi.  13, 12, 4
    mfxer   7
    mtxer   8
    sradi.  13, 12, 7
    mfxer   7
    mtxer   8
    sradi.  13, 12, 8
    mfxer   7
    mtxer   8
    sradi.  13, 12, 15
    mfxer   7
    mtxer   8
    sradi.  13, 12, 16
    mfxer   7
    mtxer   8
    sradi.  13, 12, 31
    mfxer   7
    mtxer   8
    sradi.  13, 12, 32
    mfxer   7
    mtxer   8
    sradi.  13, 12, 63
    mfxer   7
    mtxer   8

    ldu     12, 8(6)
    sradi.  13, 12, 0
    mfxer   7
    mtxer   8
    sradi.  13, 12, 1
    mfxer   7
    mtxer   8
    sradi.  13, 12, 2
    mfxer   7
    mtxer   8
    sradi.  13, 12, 3
    mfxer   7
    mtxer   8
    sradi.  13, 12, 4
    mfxer   7
    mtxer   8
    sradi.  13, 12, 7
    mfxer   7
    mtxer   8
    sradi.  13, 12, 8
    mfxer   7
    mtxer   8
    sradi.  13, 12, 15
    mfxer   7
    mtxer   8
    sradi.  13, 12, 16
    mfxer   7
    mtxer   8
    sradi.  13, 12, 31
    mfxer   7
    mtxer   8
    sradi.  13, 12, 32
    mfxer   7
    mtxer   8
    sradi.  13, 12, 63
    mfxer   7
    mtxer   8

    # Destroy stack frame
    addi    1, 1, 16
    mtlr    0

    # Return
    blr

.data
.align 3
test_data:
    .quad 0x0123456789abcdef
    .quad 0x123456789abcdef0
    .quad 0x23456789abcdef01
    .quad 0x3456789abcdef012
    .quad 0x56789abcdef01234
    .quad 0x6789abcdef012345
    .quad 0x789abcdef0123456
    .quad 0x89abcdef01234567
    .quad 0x9abcdef012345678
    .quad 0xabcdef0123456789
    .quad 0xbcdef0123456789a
    .quad 0xdef0123456789abc
    .quad 0xef0123456789abcd
