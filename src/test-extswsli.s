.machine      power8
.abiversion   2

.text
.global       _start

_start:
    # Setup stack frame
    mflr      0
    stw       0, 8(1)
    stwu      1, -16(1)

    # Run test cases
    bl        run_test

    # Call exit()
    li        0, 1  # Syscall number (sys_exit)
    li        3, 1  # First argument is exit code
    sc              # Call kernel

run_test:
    # Setup stack frame
    mflr      0
    stw       0, 8(1)
    stwu      1, -16(1)

    # Do some work
    lis       5, test_data@h
    ori       5, 5, test_data@l
    li        8, 0

    # When Rc = 0
    mr        6, 5
    ld        12, 0(6)
    extswsli  13, 12, 0
    extswsli  13, 12, 7
    extswsli  13, 12, 8
    extswsli  13, 12, 15
    extswsli  13, 12, 16
    extswsli  13, 12, 31
    extswsli  13, 12, 32
    extswsli  13, 12, 63

    ldu       12, 8(6)
    extswsli  13, 12, 0
    extswsli  13, 12, 7
    extswsli  13, 12, 8
    extswsli  13, 12, 15
    extswsli  13, 12, 16
    extswsli  13, 12, 31
    extswsli  13, 12, 32
    extswsli  13, 12, 63

    ldu       12, 8(6)
    extswsli  13, 12, 0
    extswsli  13, 12, 7
    extswsli  13, 12, 8
    extswsli  13, 12, 15
    extswsli  13, 12, 16
    extswsli  13, 12, 31
    extswsli  13, 12, 32
    extswsli  13, 12, 63

    ldu       12, 8(6)
    extswsli  13, 12, 0
    extswsli  13, 12, 7
    extswsli  13, 12, 8
    extswsli  13, 12, 15
    extswsli  13, 12, 16
    extswsli  13, 12, 31
    extswsli  13, 12, 32
    extswsli  13, 12, 63

    ldu       12, 8(6)
    extswsli  13, 12, 0
    extswsli  13, 12, 7
    extswsli  13, 12, 8
    extswsli  13, 12, 15
    extswsli  13, 12, 16
    extswsli  13, 12, 31
    extswsli  13, 12, 32
    extswsli  13, 12, 63

    ldu       12, 8(6)
    extswsli  13, 12, 0
    extswsli  13, 12, 7
    extswsli  13, 12, 8
    extswsli  13, 12, 15
    extswsli  13, 12, 16
    extswsli  13, 12, 31
    extswsli  13, 12, 32
    extswsli  13, 12, 63

    ldu       12, 8(6)
    extswsli  13, 12, 0
    extswsli  13, 12, 7
    extswsli  13, 12, 8
    extswsli  13, 12, 15
    extswsli  13, 12, 16
    extswsli  13, 12, 31
    extswsli  13, 12, 32
    extswsli  13, 12, 63

    ldu       12, 8(6)
    extswsli  13, 12, 0
    extswsli  13, 12, 7
    extswsli  13, 12, 8
    extswsli  13, 12, 15
    extswsli  13, 12, 16
    extswsli  13, 12, 31
    extswsli  13, 12, 32
    extswsli  13, 12, 63

    ldu       12, 8(6)
    extswsli  13, 12, 0
    extswsli  13, 12, 7
    extswsli  13, 12, 8
    extswsli  13, 12, 15
    extswsli  13, 12, 16
    extswsli  13, 12, 31
    extswsli  13, 12, 32
    extswsli  13, 12, 63

    ldu       12, 8(6)
    extswsli  13, 12, 0
    extswsli  13, 12, 7
    extswsli  13, 12, 8
    extswsli  13, 12, 15
    extswsli  13, 12, 16
    extswsli  13, 12, 31
    extswsli  13, 12, 32
    extswsli  13, 12, 63

    # When Rc = 1
    mr        6, 5
    ld        12, 0(6)
    extswsli. 13, 12, 0
    extswsli. 13, 12, 7
    extswsli. 13, 12, 8
    extswsli. 13, 12, 15
    extswsli. 13, 12, 16
    extswsli. 13, 12, 31
    extswsli. 13, 12, 32
    extswsli. 13, 12, 63

    ldu       12, 8(6)
    extswsli. 13, 12, 0
    extswsli. 13, 12, 7
    extswsli. 13, 12, 8
    extswsli. 13, 12, 15
    extswsli. 13, 12, 16
    extswsli. 13, 12, 31
    extswsli. 13, 12, 32
    extswsli. 13, 12, 63

    ldu       12, 8(6)
    extswsli. 13, 12, 0
    extswsli. 13, 12, 7
    extswsli. 13, 12, 8
    extswsli. 13, 12, 15
    extswsli. 13, 12, 16
    extswsli. 13, 12, 31
    extswsli. 13, 12, 32
    extswsli. 13, 12, 63

    ldu       12, 8(6)
    extswsli. 13, 12, 0
    extswsli. 13, 12, 7
    extswsli. 13, 12, 8
    extswsli. 13, 12, 15
    extswsli. 13, 12, 16
    extswsli. 13, 12, 31
    extswsli. 13, 12, 32
    extswsli. 13, 12, 63

    ldu       12, 8(6)
    extswsli. 13, 12, 0
    extswsli. 13, 12, 7
    extswsli. 13, 12, 8
    extswsli. 13, 12, 15
    extswsli. 13, 12, 16
    extswsli. 13, 12, 31
    extswsli. 13, 12, 32
    extswsli. 13, 12, 63

    ldu       12, 8(6)
    extswsli. 13, 12, 0
    extswsli. 13, 12, 7
    extswsli. 13, 12, 8
    extswsli. 13, 12, 15
    extswsli. 13, 12, 16
    extswsli. 13, 12, 31
    extswsli. 13, 12, 32
    extswsli. 13, 12, 63

    ldu       12, 8(6)
    extswsli. 13, 12, 0
    extswsli. 13, 12, 7
    extswsli. 13, 12, 8
    extswsli. 13, 12, 15
    extswsli. 13, 12, 16
    extswsli. 13, 12, 31
    extswsli. 13, 12, 32
    extswsli. 13, 12, 63

    ldu       12, 8(6)
    extswsli. 13, 12, 0
    extswsli. 13, 12, 7
    extswsli. 13, 12, 8
    extswsli. 13, 12, 15
    extswsli. 13, 12, 16
    extswsli. 13, 12, 31
    extswsli. 13, 12, 32
    extswsli. 13, 12, 63

    ldu       12, 8(6)
    extswsli. 13, 12, 0
    extswsli. 13, 12, 7
    extswsli. 13, 12, 8
    extswsli. 13, 12, 15
    extswsli. 13, 12, 16
    extswsli. 13, 12, 31
    extswsli. 13, 12, 32
    extswsli. 13, 12, 63

    ldu       12, 8(6)
    extswsli. 13, 12, 0
    extswsli. 13, 12, 7
    extswsli. 13, 12, 8
    extswsli. 13, 12, 15
    extswsli. 13, 12, 16
    extswsli. 13, 12, 31
    extswsli. 13, 12, 32
    extswsli. 13, 12, 63

    # Destroy stack frame
    addi      1, 1, 16
    mtlr      0

    # Return
    blr

.data
.align 3
test_data:
    .quad 0x0f0f0f0f0f0f0f0f
    .quad 0xf0f0f0f0f0f0f0f0
    .quad 0x00ff00ff00ff00ff
    .quad 0xff00ff00ff00ff00
    .quad 0x0000ffff0000ffff
    .quad 0xffff0000ffff0000
    .quad 0x00000000ffffffff
    .quad 0xffffffff00000000
    .quad 0x0000000000000000
    .quad 0xffffffffffffffff
