.machine    power8
.abiversion 2

.text
.global     _start

_start:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Run test cases
    bl      run_test

    # Call exit()
    li      0, 1  # Syscall number (sys_exit)
    li      3, 1  # First argument is exit code
    sc            # Call kernel

run_test:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Do some work
    lis     5, test_data@h
    ori     5, 5, test_data@l

    ld      12, 0(5)

    # When Rc = 0
    li      13, 0
    rldcr   14, 12, 13, 0
    rldcr   14, 12, 13, 7
    rldcr   14, 12, 13, 8
    rldcr   14, 12, 13, 15
    rldcr   14, 12, 13, 16
    rldcr   14, 12, 13, 31
    rldcr   14, 12, 13, 32
    rldcr   14, 12, 13, 63

    li      13, 7
    rldcr   14, 12, 13, 0
    rldcr   14, 12, 13, 7
    rldcr   14, 12, 13, 8
    rldcr   14, 12, 13, 15
    rldcr   14, 12, 13, 16
    rldcr   14, 12, 13, 31
    rldcr   14, 12, 13, 32
    rldcr   14, 12, 13, 63

    li      13, 8
    rldcr   14, 12, 13, 0
    rldcr   14, 12, 13, 7
    rldcr   14, 12, 13, 8
    rldcr   14, 12, 13, 15
    rldcr   14, 12, 13, 16
    rldcr   14, 12, 13, 31
    rldcr   14, 12, 13, 32
    rldcr   14, 12, 13, 63

    li      13, 15
    rldcr   14, 12, 13, 0
    rldcr   14, 12, 13, 7
    rldcr   14, 12, 13, 8
    rldcr   14, 12, 13, 15
    rldcr   14, 12, 13, 16
    rldcr   14, 12, 13, 31
    rldcr   14, 12, 13, 32
    rldcr   14, 12, 13, 63

    li      13, 16
    rldcr   14, 12, 13, 0
    rldcr   14, 12, 13, 7
    rldcr   14, 12, 13, 8
    rldcr   14, 12, 13, 15
    rldcr   14, 12, 13, 16
    rldcr   14, 12, 13, 31
    rldcr   14, 12, 13, 32
    rldcr   14, 12, 13, 63

    li      13, 31
    rldcr   14, 12, 13, 0
    rldcr   14, 12, 13, 7
    rldcr   14, 12, 13, 8
    rldcr   14, 12, 13, 15
    rldcr   14, 12, 13, 16
    rldcr   14, 12, 13, 31
    rldcr   14, 12, 13, 32
    rldcr   14, 12, 13, 63

    li      13, 32
    rldcr   14, 12, 13, 0
    rldcr   14, 12, 13, 7
    rldcr   14, 12, 13, 8
    rldcr   14, 12, 13, 15
    rldcr   14, 12, 13, 16
    rldcr   14, 12, 13, 31
    rldcr   14, 12, 13, 32
    rldcr   14, 12, 13, 63

    li      13, 63
    rldcr   14, 12, 13, 0
    rldcr   14, 12, 13, 7
    rldcr   14, 12, 13, 8
    rldcr   14, 12, 13, 15
    rldcr   14, 12, 13, 16
    rldcr   14, 12, 13, 31
    rldcr   14, 12, 13, 32
    rldcr   14, 12, 13, 63

    # When Rc = 1
    li      13, 0
    rldcr.  14, 12, 13, 0
    rldcr.  14, 12, 13, 7
    rldcr.  14, 12, 13, 8
    rldcr.  14, 12, 13, 15
    rldcr.  14, 12, 13, 16
    rldcr.  14, 12, 13, 31
    rldcr.  14, 12, 13, 32
    rldcr.  14, 12, 13, 63

    li      13, 7
    rldcr.  14, 12, 13, 0
    rldcr.  14, 12, 13, 7
    rldcr.  14, 12, 13, 8
    rldcr.  14, 12, 13, 15
    rldcr.  14, 12, 13, 16
    rldcr.  14, 12, 13, 31
    rldcr.  14, 12, 13, 32
    rldcr.  14, 12, 13, 63

    li      13, 8
    rldcr.  14, 12, 13, 0
    rldcr.  14, 12, 13, 7
    rldcr.  14, 12, 13, 8
    rldcr.  14, 12, 13, 15
    rldcr.  14, 12, 13, 16
    rldcr.  14, 12, 13, 31
    rldcr.  14, 12, 13, 32
    rldcr.  14, 12, 13, 63

    li      13, 15
    rldcr.  14, 12, 13, 0
    rldcr.  14, 12, 13, 7
    rldcr.  14, 12, 13, 8
    rldcr.  14, 12, 13, 15
    rldcr.  14, 12, 13, 16
    rldcr.  14, 12, 13, 31
    rldcr.  14, 12, 13, 32
    rldcr.  14, 12, 13, 63

    li      13, 16
    rldcr.  14, 12, 13, 0
    rldcr.  14, 12, 13, 7
    rldcr.  14, 12, 13, 8
    rldcr.  14, 12, 13, 15
    rldcr.  14, 12, 13, 16
    rldcr.  14, 12, 13, 31
    rldcr.  14, 12, 13, 32
    rldcr.  14, 12, 13, 63

    li      13, 31
    rldcr.  14, 12, 13, 0
    rldcr.  14, 12, 13, 7
    rldcr.  14, 12, 13, 8
    rldcr.  14, 12, 13, 15
    rldcr.  14, 12, 13, 16
    rldcr.  14, 12, 13, 31
    rldcr.  14, 12, 13, 32
    rldcr.  14, 12, 13, 63

    li      13, 32
    rldcr.  14, 12, 13, 0
    rldcr.  14, 12, 13, 7
    rldcr.  14, 12, 13, 8
    rldcr.  14, 12, 13, 15
    rldcr.  14, 12, 13, 16
    rldcr.  14, 12, 13, 31
    rldcr.  14, 12, 13, 32
    rldcr.  14, 12, 13, 63

    li      13, 63
    rldcr.  14, 12, 13, 0
    rldcr.  14, 12, 13, 7
    rldcr.  14, 12, 13, 8
    rldcr.  14, 12, 13, 15
    rldcr.  14, 12, 13, 16
    rldcr.  14, 12, 13, 31
    rldcr.  14, 12, 13, 32
    rldcr.  14, 12, 13, 63

    # Destroy stack frame
    addi    1, 1, 16
    mtlr    0

    # Return
    blr

.data
.align 3
test_data:
    .quad 0x0123456789abcdef
