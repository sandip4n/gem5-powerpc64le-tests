.machine    power8
.abiversion 2

.text
.global     _start

_start:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Run test cases
    bl      run_test

    # Call exit()
    li      0, 1  # Syscall number (sys_exit)
    li      3, 1  # First argument is exit code
    sc            # Call kernel

run_test:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Do some work
    lis     5, test_data@h
    ori     5, 5, test_data@l

    lwzu    12, 0(5)  # Positive displacements
    lwzu    12, 4(5)
    lwzu    12, 4(5)
    lwzu    12, 4(5)

    addi    5, 5, 4
    lwzu    12, -4(5) # Negative displacements
    lwzu    12, -4(5)
    lwzu    12, -4(5)
    lwzu    12, -4(5)

    # Destroy stack frame
    addi    1, 1, 16
    mtlr    0

    # Return
    blr

.data
.align 3
test_data:
    .long 0x10000000
    .long 0x20000000
    .long 0x40000000
    .long 0x80000000
