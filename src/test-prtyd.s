.machine    power8
.abiversion 2

.text
.global     _start

_start:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Run test cases
    bl      run_test

    # Call exit()
    li      0, 1  # Syscall number (sys_exit)
    li      3, 1  # First argument is exit code
    sc            # Call kernel

run_test:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Do some work
    lis     5, test_data@h
    ori     5, 5, test_data@l

    ld      12, 0(5)
    prtyd   13, 12
    ldu     12, 8(5)
    prtyd   13, 12
    ldu     12, 8(5)
    prtyd   13, 12
    ldu     12, 8(5)
    prtyd   13, 12
    ldu     12, 8(5)
    prtyd   13, 12
    ldu     12, 8(5)
    prtyd   13, 12
    ldu     12, 8(5)
    prtyd   13, 12
    ldu     12, 8(5)
    prtyd   13, 12
    ldu     12, 8(5)
    prtyd   13, 12
    ldu     12, 8(5)
    prtyd   13, 12
    ldu     12, 8(5)
    prtyd   13, 12
    ldu     12, 8(5)
    prtyd   13, 12
    ldu     12, 8(5)
    prtyd   13, 12
    ldu     12, 8(5)
    prtyd   13, 12
    ldu     12, 8(5)
    prtyd   13, 12
    ldu     12, 8(5)
    prtyd   13, 12

    # Destroy stack frame
    addi    1, 1, 16
    mtlr    0

    # Return
    blr

.data
.align 3
test_data:
    .quad 0x0000000000000000
    .quad 0x0000000ff0000000
    .quad 0x000000ffff000000
    .quad 0x00000ffffff00000
    .quad 0x0000ffffffff0000
    .quad 0x000ffffffffff000
    .quad 0x00ffffffffffff00
    .quad 0x0ffffffffffffff0
    .quad 0xf00000000000000f
    .quad 0xff000000000000ff
    .quad 0xfff0000000000fff
    .quad 0xffff00000000ffff
    .quad 0xfffff000000fffff
    .quad 0xffffff0000ffffff
    .quad 0xfffffff00fffffff
    .quad 0xffffffffffffffff
