.machine    power8
.abiversion 2

.text
.global     _start

_start:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Run test cases
    bl      run_test

    # Call exit()
    li      0, 1  # Syscall number (sys_exit)
    li      3, 1  # First argument is exit code
    sc            # Call kernel

run_test:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Do some work
    lis     5, test_data@h
    ori     5, 5, test_data@l
    addi    6, 5, 8
    li      9, 8192
    li      11, 0

    # When CA = 0 before instruction is executed
    mr      7, 5
    mr      8, 6
    ld      12, 0(7)
    ld      13, 0(8)
    subfe   14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfe   14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfe   14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfe   14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfe   14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfe   14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfe   14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfe   14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfe   14, 12, 13
    mfxer   10
    mtxer   11

    mr      7, 5
    mr      8, 6
    ld      12, 0(7)
    ld      13, 0(8)
    subfe.  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfe.  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfe.  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfe.  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfe.  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfe.  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfe.  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfe.  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfe.  14, 12, 13
    mfxer   10
    mtxer   11

    mr      7, 5
    mr      8, 6
    ld      12, 0(7)
    ld      13, 0(8)
    subfeo  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfeo  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfeo  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfeo  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfeo  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfeo  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfeo  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfeo  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfeo  14, 12, 13
    mfxer   10
    mtxer   11

    mr      7, 5
    mr      8, 6
    ld      12, 0(7)
    ld      13, 0(8)
    subfeo. 14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfeo. 14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfeo. 14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfeo. 14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfeo. 14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfeo. 14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfeo. 14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfeo. 14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfeo. 14, 12, 13
    mfxer   10
    mtxer   11

    # When CA = 1 before instruction is executed
    mr      7, 5
    mr      8, 6
    ld      12, 0(7)
    ld      13, 0(8)
    mtxer   9
    subfe   14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfe   14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfe   14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfe   14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfe   14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfe   14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfe   14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfe   14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfe   14, 12, 13
    mfxer   10
    mtxer   11

    mr      7, 5
    mr      8, 6
    ld      12, 0(7)
    ld      13, 0(8)
    mtxer   9
    subfe.  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfe.  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfe.  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    subfe.  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfe.  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfe.  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfe.  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfe.  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfe.  14, 12, 13
    mfxer   10
    mtxer   11

    mr      7, 5
    mr      8, 6
    ld      12, 0(7)
    ld      13, 0(8)
    mtxer   9
    subfeo  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfeo  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfeo  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfeo  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfeo  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfeo  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfeo  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfeo  14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfeo  14, 12, 13
    mfxer   10
    mtxer   11

    mr      7, 5
    mr      8, 6
    ld      12, 0(7)
    ld      13, 0(8)
    mtxer   9
    subfeo. 14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfeo. 14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfeo. 14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfeo. 14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfeo. 14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfeo. 14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfeo. 14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfeo. 14, 12, 13
    mfxer   10
    mtxer   11
    ldu     12, 16(7)
    ldu     13, 16(8)
    mtxer   9
    subfeo. 14, 12, 13
    mfxer   10
    mtxer   11

    # Destroy stack frame
    addi    1, 1, 16
    mtlr    0

    # Return
    blr

.data
.align 3
test_data:
    .quad 0x0000000000000001, 0x0000000000000001
    .quad 0x0000000000000001, 0x0000000080000000
    .quad 0x0000000000000001, 0x00000000ffffffff
    .quad 0x0000000100000000, 0x0000000100000000
    .quad 0x0000000100000000, 0x8000000000000000
    .quad 0x0000000100000000, 0xffffffff00000000
    .quad 0x0000000100000001, 0x0000000100000001
    .quad 0x0000000100000001, 0x8000000080000000
    .quad 0x0000000100000001, 0xffffffffffffffff
