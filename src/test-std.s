.machine    power8
.abiversion 2

.text
.global     _start

_start:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Run test cases
    bl      run_test

    # Call exit()
    li      0, 1  # Syscall number (sys_exit)
    li      3, 1  # First argument is exit code
    sc            # Call kernel

run_test:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Do some work
    lis     5, test_data@h
    ori     5, 5, test_data@l
    mr      6, 5
    addi    6, 6, 32

    ld      12, 0(5)   # Positive displacements
    std     12, 0(6)
    ld      12, 0(5)
    ld      12, 8(5)
    std     12, 8(6)
    ld      12, 8(6)
    ld      12, 16(5)
    std     12, 16(6)
    ld      12, 16(6)
    ld      12, 24(5)
    std     12, 24(6)
    ld      12, 24(6)

    addi    5, 5, 32
    addi    6, 6, 32
    ld      12, -32(5) # Negative displacements
    std     12, -32(6)
    ld      12, -32(6)
    ld      12, -24(5)
    std     12, -24(6)
    ld      12, -24(6)
    ld      12, -16(5)
    std     12, -16(6)
    ld      12, -16(6)
    ld      12, -8(5)
    std     12, -8(6)
    ld      12, -8(6)

    std     12, 8(0)   # This store will fail but the point to observe here is
                       # that the effective address (EA) must be calculated
                       # properly. In this case, RA = 0. So, the calculation
                       # should be EA = 0 + D. Otherwise, it is EA = (RA) + D.

    # Destroy stack frame
    addi    1, 1, 16
    mtlr    0

    # Return
    blr

.data
.align 3
test_data:
    .quad 0x1000000000000000
    .quad 0x2000000000000000
    .quad 0x4000000000000000
    .quad 0x8000000000000000
