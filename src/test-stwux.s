.machine    power8
.abiversion 2

.text
.global     _start

_start:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Run test cases
    bl      run_test

    # Call exit()
    li      0, 1  # Syscall number (sys_exit)
    li      3, 1  # First argument is exit code
    sc            # Call kernel

run_test:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Do some work
    lis     5, test_data@h
    ori     5, 5, test_data@l
    lis     6, test_disp@h
    ori     6, 6, test_disp@l
    mr      7, 6
    addi    7, 7, 16
    li      9, 0

    subi    5, 5, 4
    subi    7, 7, 4
    ld      8, 0(6)  # Positive displacements
    lwzux   12, 5, 8
    stwux   12, 7, 8
    lwzux   12, 7, 9
    lwzux   12, 5, 8
    stwux   12, 7, 8
    lwzux   12, 7, 9
    lwzux   12, 5, 8
    stwux   12, 7, 8
    lwzux   12, 7, 9
    lwzux   12, 5, 8
    stwux   12, 7, 8
    lwzux   12, 7, 9

    addi    5, 5, 4
    addi    7, 7, 4
    ld      8, 8(6)  # Negative displacements
    lwzux   12, 5, 8
    stwux   12, 7, 8
    lwzux   12, 7, 9
    lwzux   12, 5, 8
    stwux   12, 7, 8
    lwzux   12, 7, 9
    lwzux   12, 5, 8
    stwux   12, 7, 8
    lwzux   12, 7, 9
    lwzux   12, 5, 8
    stwux   12, 7, 8
    lwzux   12, 7, 9

    # Destroy stack frame
    addi    1, 1, 16
    mtlr    0

    # Return
    blr

.data
.align 3
test_data:
    .long 0x10000000
    .long 0x20000000
    .long 0x40000000
    .long 0x80000000

test_disp:
    .quad 0x0000000000000004
    .quad 0xfffffffffffffffc
