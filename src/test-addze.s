.machine    power8
.abiversion 2

.text
.global     _start

_start:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Run test cases
    bl      run_test

    # Call exit()
    li      0, 1  # Syscall number (sys_exit)
    li      3, 1  # First argument is exit code
    sc            # Call kernel

run_test:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Do some work
    lis     5, test_data@h
    ori     5, 5, test_data@l
    lis     6, 8192
    li      8, 0

    # When CA = 0 before instruction is executed
    mr      9, 5
    ld      12, 0(9)
    addze   13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    addze   13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    addze   13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    addze   13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    addze   13, 12
    mfxer   7
    mtxer   8

    mr      9, 5
    ld      12, 0(9)
    addze.  13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    addze.  13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    addze.  13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    addze.  13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    addze.  13, 12
    mfxer   7
    mtxer   8

    mr      9, 5
    ld      12, 0(9)
    addzeo  13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    addzeo  13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    addzeo  13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    addzeo  13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    addzeo  13, 12
    mfxer   7
    mtxer   8

    mr      9, 5
    ld      12, 0(9)
    addzeo. 13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    addzeo. 13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    addzeo. 13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    addzeo. 13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    addzeo. 13, 12
    mfxer   7
    mtxer   8

    # When CA = 1 before instruction is executed
    mr      9, 5
    ld      12, 0(9)
    mtxer   6
    addze   13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    mtxer   6
    addze   13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    mtxer   6
    addze   13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    mtxer   6
    addze   13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    mtxer   6
    addze   13, 12
    mfxer   7
    mtxer   8

    mr      9, 5
    ld      12, 0(9)
    mtxer   6
    addze.  13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    mtxer   6
    addze.  13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    mtxer   6
    addze.  13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    mtxer   6
    addze.  13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    mtxer   6
    addze.  13, 12
    mfxer   7
    mtxer   8

    mr      9, 5
    ld      12, 0(9)
    mtxer   6
    addzeo  13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    mtxer   6
    addzeo  13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    mtxer   6
    addzeo  13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    mtxer   6
    addzeo  13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    mtxer   6
    addzeo  13, 12
    mfxer   7
    mtxer   8

    mr      9, 5
    ld      12, 0(9)
    mtxer   6
    addzeo. 13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    mtxer   6
    addzeo. 13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    mtxer   6
    addzeo. 13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    mtxer   6
    addzeo. 13, 12
    mfxer   7
    mtxer   8
    ldu     12, 8(9)
    mtxer   6
    addzeo. 13, 12
    mfxer   7
    mtxer   8

    # Destroy stack frame
    addi    1, 1, 16
    mtlr    0

    # Return
    blr

.data
.align 3
test_data:
    .quad 0x0000000080000000
    .quad 0x000000007fffffff
    .quad 0x00000000ffffffff
    .quad 0x7fffffffffffffff
    .quad 0xffffffffffffffff
