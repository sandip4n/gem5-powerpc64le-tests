.machine    power8
.abiversion 2

.text
.global     _start

_start:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Run test cases
    bl      run_test

    # Call exit()
    li      0, 1  # Syscall number (sys_exit)
    li      3, 1  # First argument is exit code
    sc            # Call kernel

run_test:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Do some work
    lis     5, test_data@h
    ori     5, 5, test_data@l

    ld      12, 0(5)

    # When Rc = 0
    rldicr  13, 12, 0, 0
    rldicr  13, 12, 0, 7
    rldicr  13, 12, 0, 8
    rldicr  13, 12, 0, 15
    rldicr  13, 12, 0, 16
    rldicr  13, 12, 0, 31
    rldicr  13, 12, 0, 32
    rldicr  13, 12, 0, 63
    rldicr  13, 12, 7, 0
    rldicr  13, 12, 7, 7
    rldicr  13, 12, 7, 8
    rldicr  13, 12, 7, 15
    rldicr  13, 12, 7, 16
    rldicr  13, 12, 7, 31
    rldicr  13, 12, 7, 32
    rldicr  13, 12, 7, 63
    rldicr  13, 12, 8, 0
    rldicr  13, 12, 8, 7
    rldicr  13, 12, 8, 8
    rldicr  13, 12, 8, 15
    rldicr  13, 12, 8, 16
    rldicr  13, 12, 8, 31
    rldicr  13, 12, 8, 32
    rldicr  13, 12, 8, 63
    rldicr  13, 12, 15, 0
    rldicr  13, 12, 15, 7
    rldicr  13, 12, 15, 8
    rldicr  13, 12, 15, 15
    rldicr  13, 12, 15, 16
    rldicr  13, 12, 15, 31
    rldicr  13, 12, 15, 32
    rldicr  13, 12, 15, 63
    rldicr  13, 12, 16, 0
    rldicr  13, 12, 16, 7
    rldicr  13, 12, 16, 8
    rldicr  13, 12, 16, 15
    rldicr  13, 12, 16, 16
    rldicr  13, 12, 16, 31
    rldicr  13, 12, 16, 32
    rldicr  13, 12, 16, 63
    rldicr  13, 12, 31, 0
    rldicr  13, 12, 31, 7
    rldicr  13, 12, 31, 8
    rldicr  13, 12, 31, 15
    rldicr  13, 12, 31, 16
    rldicr  13, 12, 31, 31
    rldicr  13, 12, 31, 32
    rldicr  13, 12, 31, 63
    rldicr  13, 12, 32, 0
    rldicr  13, 12, 32, 7
    rldicr  13, 12, 32, 8
    rldicr  13, 12, 32, 15
    rldicr  13, 12, 32, 16
    rldicr  13, 12, 32, 31
    rldicr  13, 12, 32, 32
    rldicr  13, 12, 32, 63
    rldicr  13, 12, 63, 0
    rldicr  13, 12, 63, 7
    rldicr  13, 12, 63, 8
    rldicr  13, 12, 63, 15
    rldicr  13, 12, 63, 16
    rldicr  13, 12, 63, 31
    rldicr  13, 12, 63, 32
    rldicr  13, 12, 63, 63

    # When Rc = 1
    rldicr. 13, 12, 0, 0
    rldicr. 13, 12, 0, 7
    rldicr. 13, 12, 0, 8
    rldicr. 13, 12, 0, 15
    rldicr. 13, 12, 0, 16
    rldicr. 13, 12, 0, 31
    rldicr. 13, 12, 0, 32
    rldicr. 13, 12, 0, 63
    rldicr. 13, 12, 7, 0
    rldicr. 13, 12, 7, 7
    rldicr. 13, 12, 7, 8
    rldicr. 13, 12, 7, 15
    rldicr. 13, 12, 7, 16
    rldicr. 13, 12, 7, 31
    rldicr. 13, 12, 7, 32
    rldicr. 13, 12, 7, 63
    rldicr. 13, 12, 8, 0
    rldicr. 13, 12, 8, 7
    rldicr. 13, 12, 8, 8
    rldicr. 13, 12, 8, 15
    rldicr. 13, 12, 8, 16
    rldicr. 13, 12, 8, 31
    rldicr. 13, 12, 8, 32
    rldicr. 13, 12, 8, 63
    rldicr. 13, 12, 15, 0
    rldicr. 13, 12, 15, 7
    rldicr. 13, 12, 15, 8
    rldicr. 13, 12, 15, 15
    rldicr. 13, 12, 15, 16
    rldicr. 13, 12, 15, 31
    rldicr. 13, 12, 15, 32
    rldicr. 13, 12, 15, 63
    rldicr. 13, 12, 16, 0
    rldicr. 13, 12, 16, 7
    rldicr. 13, 12, 16, 8
    rldicr. 13, 12, 16, 15
    rldicr. 13, 12, 16, 16
    rldicr. 13, 12, 16, 31
    rldicr. 13, 12, 16, 32
    rldicr. 13, 12, 16, 63
    rldicr. 13, 12, 31, 0
    rldicr. 13, 12, 31, 7
    rldicr. 13, 12, 31, 8
    rldicr. 13, 12, 31, 15
    rldicr. 13, 12, 31, 16
    rldicr. 13, 12, 31, 31
    rldicr. 13, 12, 31, 32
    rldicr. 13, 12, 31, 63
    rldicr. 13, 12, 32, 0
    rldicr. 13, 12, 32, 7
    rldicr. 13, 12, 32, 8
    rldicr. 13, 12, 32, 15
    rldicr. 13, 12, 32, 16
    rldicr. 13, 12, 32, 31
    rldicr. 13, 12, 32, 32
    rldicr. 13, 12, 32, 63
    rldicr. 13, 12, 63, 0
    rldicr. 13, 12, 63, 7
    rldicr. 13, 12, 63, 8
    rldicr. 13, 12, 63, 15
    rldicr. 13, 12, 63, 16
    rldicr. 13, 12, 63, 31
    rldicr. 13, 12, 63, 32
    rldicr. 13, 12, 63, 63

    # Destroy stack frame
    addi    1, 1, 16
    mtlr    0

    # Return
    blr

.data
.align 3
test_data:
    .quad 0x0123456789abcdef
