.machine    power8
.abiversion 2

.text
.global     _start

_start:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Run test cases
    bl      run_test

    # Call exit()
    li      0, 1  # Syscall number (sys_exit)
    li      3, 1  # First argument is exit code
    sc            # Call kernel

run_test:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Do some work
    lis     13, 2313
    ori     13, 13, 2314
    rldicr  13, 13, 32, 31
    oris    13, 13, 2828
    ori     13, 13, 3360

    li      12, 0
    cmpeqb  0, 12, 13
    li      12, 9
    cmpeqb  1, 12, 13
    li      12, 10
    cmpeqb  2, 12, 13
    li      12, 11
    cmpeqb  3, 12, 13
    li      12, 12
    cmpeqb  4, 12, 13
    li      12, 13
    cmpeqb  5, 12, 13
    li      12, 32
    cmpeqb  6, 12, 13
    li      12, 48
    cmpeqb  7, 12, 13
    li      12, 57
    cmpeqb  0, 12, 13
    li      12, 65
    cmpeqb  1, 12, 13
    li      12, 90
    cmpeqb  2, 12, 13
    li      12, 97
    cmpeqb  3, 12, 13
    li      12, 122
    cmpeqb  4, 12, 13
    li      12, 133
    cmpeqb  5, 12, 13
    li      12, 160
    cmpeqb  6, 12, 13
    li      12, 255
    cmpeqb  7, 12, 13

    # Destroy stack frame
    addi    1, 1, 16
    mtlr    0

    # Return
    blr
