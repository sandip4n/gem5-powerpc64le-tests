.machine    power8
.abiversion 2

.text
.global     _start

_start:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Run test cases
    bl      run_test

    # Call exit()
    li      0, 1  # Syscall number (sys_exit)
    li      3, 1  # First argument is exit code
    sc            # Call kernel

run_test:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Do some work
    lis     5, test_data@h
    ori     5, 5, test_data@l

    ld      12, 0(5)
    ldu     13, 8(5)
    ldu     14, 8(5)
    maddld  15, 12, 13, 14
    ldu     12, 8(5)
    ldu     13, 8(5)
    ldu     14, 8(5)
    maddld  15, 12, 13, 14
    ldu     12, 8(5)
    ldu     13, 8(5)
    ldu     14, 8(5)
    maddld  15, 12, 13, 14
    ldu     12, 8(5)
    ldu     13, 8(5)
    ldu     14, 8(5)
    maddld  15, 12, 13, 14
    ldu     12, 8(5)
    ldu     13, 8(5)
    ldu     14, 8(5)
    maddld  15, 12, 13, 14
    ldu     12, 8(5)
    ldu     13, 8(5)
    ldu     14, 8(5)
    maddld  15, 12, 13, 14
    ldu     12, 8(5)
    ldu     13, 8(5)
    ldu     14, 8(5)
    maddld  15, 12, 13, 14
    ldu     12, 8(5)
    ldu     13, 8(5)
    ldu     14, 8(5)
    maddld  15, 12, 13, 14
    ldu     12, 8(5)
    ldu     13, 8(5)
    ldu     14, 8(5)
    maddld  15, 12, 13, 14
    ldu     12, 8(5)
    ldu     13, 8(5)
    ldu     14, 8(5)
    maddld  15, 12, 13, 14
    ldu     12, 8(5)
    ldu     13, 8(5)
    ldu     14, 8(5)
    maddld  15, 12, 13, 14
    ldu     12, 8(5)
    ldu     13, 8(5)
    ldu     14, 8(5)
    maddld  15, 12, 13, 14
    ldu     12, 8(5)
    ldu     13, 8(5)
    ldu     14, 8(5)
    maddld  15, 12, 13, 14
    ldu     12, 8(5)
    ldu     13, 8(5)
    ldu     14, 8(5)
    maddld  15, 12, 13, 14
    ldu     12, 8(5)
    ldu     13, 8(5)
    ldu     14, 8(5)
    maddld  15, 12, 13, 14
    ldu     12, 8(5)
    ldu     13, 8(5)
    ldu     14, 8(5)
    maddld  15, 12, 13, 14
    ldu     12, 8(5)
    ldu     13, 8(5)
    ldu     14, 8(5)
    maddld  15, 12, 13, 14
    ldu     12, 8(5)
    ldu     13, 8(5)
    ldu     14, 8(5)
    maddld  15, 12, 13, 14

    # Destroy stack frame
    addi    1, 1, 16
    mtlr    0

    # Return
    blr

.data
.align 3
test_data:
    .quad 0x000000007fffffff, 0x000000007fffffff, 0x000000007fffffff
    .quad 0x0000000080000000, 0x0000000080000000, 0x0000000080000000
    .quad 0x00000000ffffffff, 0x00000000ffffffff, 0x00000000ffffffff
    .quad 0x7fffffff00000000, 0x000000007fffffff, 0x7fffffffffffffff
    .quad 0x8000000000000000, 0x0000000080000000, 0x8000000000000000
    .quad 0xffffffff00000000, 0x00000000ffffffff, 0xffffffffffffffff
    .quad 0x7fffffff00000000, 0x7fffffff00000000, 0x7fffffffffffffff
    .quad 0x8000000000000000, 0x8000000000000000, 0x8000000000000000
    .quad 0xffffffff00000000, 0xffffffff00000000, 0xffffffffffffffff
    .quad 0x7fffffff7fffffff, 0x000000007fffffff, 0x7fffffffffffffff
    .quad 0x8000000080000000, 0x0000000080000000, 0x8000000000000000
    .quad 0xffffffffffffffff, 0x00000000ffffffff, 0xffffffffffffffff
    .quad 0x7fffffff7fffffff, 0x7fffffff00000000, 0x7fffffffffffffff
    .quad 0x8000000080000000, 0x8000000000000000, 0x8000000000000000
    .quad 0xffffffffffffffff, 0xffffffff00000000, 0xffffffffffffffff
    .quad 0x7fffffff7fffffff, 0x7fffffff7fffffff, 0x7fffffffffffffff
    .quad 0x8000000080000000, 0x8000000080000000, 0x8000000000000000
    .quad 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff
