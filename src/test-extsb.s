.machine    power8
.abiversion 2

.text
.global     _start

_start:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Run test cases
    bl      run_test

    # Call exit()
    li      0, 1  # Syscall number (sys_exit)
    li      3, 1  # First argument is exit code
    sc            # Call kernel

run_test:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Do some work
    lis     5, test_data@h
    ori     5, 5, test_data@l

    mr      6, 5
    ld      12, 0(6)
    extsb   13, 12
    ldu     12, 8(6)
    extsb   13, 12
    ldu     12, 8(6)
    extsb   13, 12
    ldu     12, 8(6)
    extsb   13, 12
    ldu     12, 8(6)
    extsb   13, 12
    ldu     12, 8(6)
    extsb   13, 12
    ldu     12, 8(6)
    extsb   13, 12
    ldu     12, 8(6)
    extsb   13, 12
    ldu     12, 8(6)
    extsb   13, 12
    ldu     12, 8(6)
    extsb   13, 12
    ldu     12, 8(6)
    extsb   13, 12
    ldu     12, 8(6)
    extsb   13, 12
    ldu     12, 8(6)
    extsb   13, 12
    ldu     12, 8(6)
    extsb   13, 12
    ldu     12, 8(6)
    extsb   13, 12
    ldu     12, 8(6)
    extsb   13, 12
    ldu     12, 8(6)
    extsb   13, 12
    ldu     12, 8(6)
    extsb   13, 12
    ldu     12, 8(6)
    extsb   13, 12
    ldu     12, 8(6)
    extsb   13, 12

    mr      6, 5
    ld      12, 0(6)
    extsb.  13, 12
    ldu     12, 8(6)
    extsb.  13, 12
    ldu     12, 8(6)
    extsb.  13, 12
    ldu     12, 8(6)
    extsb.  13, 12
    ldu     12, 8(6)
    extsb.  13, 12
    ldu     12, 8(6)
    extsb.  13, 12
    ldu     12, 8(6)
    extsb.  13, 12
    ldu     12, 8(6)
    extsb.  13, 12
    ldu     12, 8(6)
    extsb.  13, 12
    ldu     12, 8(6)
    extsb.  13, 12
    ldu     12, 8(6)
    extsb.  13, 12
    ldu     12, 8(6)
    extsb.  13, 12
    ldu     12, 8(6)
    extsb.  13, 12
    ldu     12, 8(6)
    extsb.  13, 12
    ldu     12, 8(6)
    extsb.  13, 12
    ldu     12, 8(6)
    extsb.  13, 12
    ldu     12, 8(6)
    extsb.  13, 12
    ldu     12, 8(6)
    extsb.  13, 12
    ldu     12, 8(6)
    extsb.  13, 12
    ldu     12, 8(6)
    extsb.  13, 12

    # Destroy stack frame
    addi    1, 1, 16
    mtlr    0

    # Return
    blr

.data
.align 3
test_data:
    .quad 0x0000000000000000
    .quad 0x0000000000000010
    .quad 0x0000000000000020
    .quad 0x0000000000000040
    .quad 0x0000000000000080
    .quad 0x000000000000ff00
    .quad 0x000000000000ff10
    .quad 0x000000000000ff20
    .quad 0x000000000000ff40
    .quad 0x000000000000ff80
    .quad 0x00000000ffffff00
    .quad 0x00000000ffffff10
    .quad 0x00000000ffffff20
    .quad 0x00000000ffffff40
    .quad 0x00000000ffffff80
    .quad 0xffffffffffffff00
    .quad 0xffffffffffffff10
    .quad 0xffffffffffffff20
    .quad 0xffffffffffffff40
    .quad 0xffffffffffffff80
