.machine    power8
.abiversion 2

.text
.global     _start

_start:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Run test cases
    bl      run_test

    # Call exit()
    li      0, 1  # Syscall number (sys_exit)
    li      3, 1  # First argument is exit code
    sc            # Call kernel

run_test:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Do some work
    lis     5, test_data@h
    ori     5, 5, test_data@l
    mr      6, 5
    addi    6, 6, 4

    lbz     12, 0(5)  # Positive displacements
    stb     12, 0(6)
    lbz     12, 0(6)
    lbz     12, 1(5)
    stb     12, 1(6)
    lbz     12, 1(6)
    lbz     12, 2(5)
    stb     12, 2(6)
    lbz     12, 2(6)
    lbz     12, 3(5)
    stb     12, 3(6)
    lbz     12, 3(6)

    addi    5, 5, 4
    addi    6, 6, 4
    lbz     12, -4(5) # Negative displacements
    stb     12, -4(6)
    lbz     12, -4(6)
    lbz     12, -3(5)
    stb     12, -3(6)
    lbz     12, -3(6)
    lbz     12, -2(5)
    stb     12, -2(6)
    lbz     12, -2(6)
    lbz     12, -1(5)
    stb     12, -1(6)
    lbz     12, -1(6)

    stb     12, 1(0)  # This store will fail but the point to observe here is
                      # that the effective address (EA) must be calculated
                      # properly. In this case, RA = 0. So, the calculation
                      # should be EA = 0 + D. Otherwise, it is EA = (RA) + D.

    # Destroy stack frame
    addi    1, 1, 16
    mtlr    0

    # Return
    blr

.data
.align 3
test_data:
    .byte 0x10
    .byte 0x20
    .byte 0x40
    .byte 0x80
