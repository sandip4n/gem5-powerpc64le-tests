.machine    power8
.abiversion 2

.text
.global     _start

_start:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Run test cases
    bl      run_test

    # Call exit()
    li      0, 1  # Syscall number (sys_exit)
    li      3, 1  # First argument is exit code
    sc            # Call kernel

run_test:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Do some work
    lis     5, test_data@h
    ori     5, 5, test_data@l
    lis     6, test_disp@h
    ori     6, 6, test_disp@l

    subi    5, 5, 2
    ld      7, 0(6)  # Positive displacements
    lhzux   12, 5, 7
    lhzux   12, 5, 7
    lhzux   12, 5, 7
    lhzux   12, 5, 7

    addi    5, 5, 2
    ld      7, 8(6)  # Negative displacements
    lhzux   12, 5, 7
    lhzux   12, 5, 7
    lhzux   12, 5, 7
    lhzux   12, 5, 7

    # Destroy stack frame
    addi    1, 1, 16
    mtlr    0

    # Return
    blr

.data
.align 3
test_data:
    .short 0x1000
    .short 0x2000
    .short 0x4000
    .short 0x8000

test_disp:
    .quad 0x0000000000000002
    .quad 0xfffffffffffffffe
