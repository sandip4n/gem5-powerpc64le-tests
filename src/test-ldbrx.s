.machine    power8
.abiversion 2

.text
.global     _start

_start:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Run test cases
    bl      run_test

    # Call exit()
    li      0, 1  # Syscall number (sys_exit)
    li      3, 1  # First argument is exit code
    sc            # Call kernel

run_test:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Do some work
    lis     5, test_data@h
    ori     5, 5, test_data@l
    lis     6, test_disp@h
    ori     6, 6, test_disp@l

    # When RA = base address, RB = displacement, EA = (RA) + (RB)
    subi    5, 5, 8
    ld      7, 0(6)   # Positive displacement
    ldbrx   12, 5, 7
    addi    7, 7, 8
    ldbrx   12, 5, 7
    addi    7, 7, 8
    ldbrx   12, 5, 7
    addi    7, 7, 8
    ldbrx   12, 5, 7

    addi    5, 5, 40
    ld      7, 8(6)   # Negative displacement
    ldbrx   12, 5, 7
    subi    7, 7, 8
    ldbrx   12, 5, 7
    subi    7, 7, 8
    ldbrx   12, 5, 7
    subi    7, 7, 8
    ldbrx   12, 5, 7

    # When RA = displacement, RB = base address, EA = (RA) + (RB)
    subi    5, 5, 40
    ld      7, 0(6)   # Positive displacement
    ldbrx   12, 7, 5
    addi    7, 7, 8
    ldbrx   12, 7, 5
    addi    7, 7, 8
    ldbrx   12, 7, 5
    addi    7, 7, 8
    ldbrx   12, 7, 5

    addi    5, 5, 40
    ld      7, 8(6)   # Negative displacement
    ldbrx   12, 7, 5
    subi    7, 7, 8
    ldbrx   12, 7, 5
    subi    7, 7, 8
    ldbrx   12, 7, 5
    subi    7, 7, 8
    ldbrx   12, 7, 5

    # When RA = 0, EA = 0 + (RB)
    subi    5, 5, 32
    ldbrx   12, 0, 5
    addi    5, 5, 8
    ldbrx   12, 0, 5
    addi    5, 5, 8
    ldbrx   12, 0, 5
    addi    5, 5, 8
    ldbrx   12, 0, 5

    # Destroy stack frame
    addi    1, 1, 16
    mtlr    0

    # Return
    blr

.data
.align 3
test_data:
    .quad 0x123456789abcdef0
    .quad 0x23456789abcdef01
    .quad 0x456789abcdef0123
    .quad 0x89abcdef01234567

test_disp:
    .quad 0x0000000000000008
    .quad 0xfffffffffffffff8
