.machine    power8
.abiversion 2

.text
.global     _start

_start:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Run test cases
    bl      run_test

    # Call exit()
    li      0, 1  # Syscall number (sys_exit)
    li      3, 1  # First argument is exit code
    sc            # Call kernel

run_test:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Do some work
    lis     5, test_data@h
    ori     5, 5, test_data@l

    lha     12, 0(5)  # Positive displacements
    lha     12, 2(5)
    lha     12, 4(5)
    lha     12, 6(5)

    addi    5, 5, 8
    lha     12, -8(5) # Negative displacements
    lha     12, -6(5)
    lha     12, -4(5)
    lha     12, -2(5)

    lha     12, 2(0)  # This load will fail but the point to observe here is
                      # that the effective address (EA) must be calculated
                      # properly. In this case, RA = 0. So, the calculation
                      # should be EA = 0 + D. Otherwise, it is EA = (RA) + D.

    # Destroy stack frame
    addi    1, 1, 16
    mtlr    0

    # Return
    blr

.data
.align 3
test_data:
    .short 0x1000
    .short 0x2000
    .short 0x4000
    .short 0x8000
