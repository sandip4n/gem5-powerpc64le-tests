.machine    power8
.abiversion 2

.text
.global     _start

_start:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Run test cases
    bl      run_test

    # Call exit()
    li      0, 1  # Syscall number (sys_exit)
    li      3, 1  # First argument is exit code
    sc            # Call kernel

run_test:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Do some work
    lis     5, test_data@h
    ori     5, 5, test_data@l
    addi    6, 5, 8

    # When L = 0
    mr      7, 5
    mr      8, 6
    ld      12, 0(7)
    ld      13, 0(8)
    cmp     0, 0, 12, 13
    ldu     12, 16(7)
    ldu     13, 16(8)
    cmp     1, 0, 12, 13
    ldu     12, 16(7)
    ldu     13, 16(8)
    cmp     2, 0, 12, 13
    ldu     12, 16(7)
    ldu     13, 16(8)
    cmp     3, 0, 12, 13
    ldu     12, 16(7)
    ldu     13, 16(8)
    cmp     4, 0, 12, 13
    ldu     12, 16(7)
    ldu     13, 16(8)
    cmp     5, 0, 12, 13
    ldu     12, 16(7)
    ldu     13, 16(8)
    cmp     6, 0, 12, 13
    ldu     12, 16(7)
    ldu     13, 16(8)
    cmp     7, 0, 12, 13
    ldu     12, 16(7)
    ldu     13, 16(8)
    cmp     0, 0, 12, 13
    ldu     12, 16(7)
    ldu     13, 16(8)
    cmp     1, 0, 12, 13
    ldu     12, 16(7)
    ldu     13, 16(8)
    cmp     2, 0, 12, 13
    ldu     12, 16(7)
    ldu     13, 16(8)
    cmp     3, 0, 12, 13
    ldu     12, 16(7)
    ldu     13, 16(8)
    cmp     4, 0, 12, 13
    ldu     12, 16(7)
    ldu     13, 16(8)
    cmp     5, 0, 12, 13
    ldu     12, 16(7)
    ldu     13, 16(8)
    cmp     6, 0, 12, 13
    ldu     12, 16(7)
    ldu     13, 16(8)
    cmp     7, 0, 12, 13

    # When L = 1
    mr      7, 5
    mr      8, 6
    ld      12, 0(7)
    ld      13, 0(8)
    cmp     0, 1, 12, 13
    ldu     12, 16(7)
    ldu     13, 16(8)
    cmp     1, 1, 12, 13
    ldu     12, 16(7)
    ldu     13, 16(8)
    cmp     2, 1, 12, 13
    ldu     12, 16(7)
    ldu     13, 16(8)
    cmp     3, 1, 12, 13
    ldu     12, 16(7)
    ldu     13, 16(8)
    cmp     4, 1, 12, 13
    ldu     12, 16(7)
    ldu     13, 16(8)
    cmp     5, 1, 12, 13
    ldu     12, 16(7)
    ldu     13, 16(8)
    cmp     6, 1, 12, 13
    ldu     12, 16(7)
    ldu     13, 16(8)
    cmp     7, 1, 12, 13
    ldu     12, 16(7)
    ldu     13, 16(8)
    cmp     0, 1, 12, 13
    ldu     12, 16(7)
    ldu     13, 16(8)
    cmp     1, 1, 12, 13
    ldu     12, 16(7)
    ldu     13, 16(8)
    cmp     2, 1, 12, 13
    ldu     12, 16(7)
    ldu     13, 16(8)
    cmp     3, 1, 12, 13
    ldu     12, 16(7)
    ldu     13, 16(8)
    cmp     4, 1, 12, 13
    ldu     12, 16(7)
    ldu     13, 16(8)
    cmp     5, 1, 12, 13
    ldu     12, 16(7)
    ldu     13, 16(8)
    cmp     6, 1, 12, 13
    ldu     12, 16(7)
    ldu     13, 16(8)
    cmp     7, 1, 12, 13

    # Destroy stack frame
    addi    1, 1, 16
    mtlr    0

    # Return
    blr

.data
.align 3
test_data:
    .quad 0x0000000000000000, 0x0000000000000000
    .quad 0x0000000000000001, 0x0000000000000001
    .quad 0x000000007fffffff, 0x000000007fffffff
    .quad 0x0000000080000000, 0x0000000080000000
    .quad 0x00000000ffffffff, 0x00000000ffffffff
    .quad 0x7fffffff00000000, 0x7fffffffffffffff
    .quad 0x8000000000000000, 0x8000000000000000
    .quad 0xffffffff00000000, 0xffffffffffffffff
    .quad 0xffffffff00000000, 0x0000000000000000
    .quad 0xffffffff00000001, 0x0000000000000001
    .quad 0xffffffff7fffffff, 0x000000007fffffff
    .quad 0xffffffff80000000, 0x0000000080000000
    .quad 0xffffffffffffffff, 0x00000000ffffffff
    .quad 0x7fffffffffffffff, 0x7fffffffffffffff
    .quad 0x8000000000000000, 0x8000000000000000
    .quad 0xffffffffffffffff, 0xffffffffffffffff
