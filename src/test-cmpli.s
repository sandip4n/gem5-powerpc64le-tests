.machine    power8
.abiversion 2

.text
.global     _start

_start:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Run test cases
    bl      run_test

    # Call exit()
    li      0, 1  # Syscall number (sys_exit)
    li      3, 1  # First argument is exit code
    sc            # Call kernel

run_test:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Do some work
    lis     5, test_data@h
    ori     5, 5, test_data@l

    # When L = 0
    mr      6, 5
    ld      12, 0(6)
    cmpli   0, 0, 12, 3
    cmpli   1, 0, 12, 15
    cmpli   2, 0, 12, 255
    cmpli   3, 0, 12, 32767
    cmpli   4, 0, 12, 32768
    cmpli   5, 0, 12, 65280
    cmpli   6, 0, 12, 65520
    cmpli   7, 0, 12, 65532
    ldu     12, 8(6)
    cmpli   0, 0, 12, 3
    cmpli   1, 0, 12, 15
    cmpli   2, 0, 12, 255
    cmpli   3, 0, 12, 32767
    cmpli   4, 0, 12, 32768
    cmpli   5, 0, 12, 65280
    cmpli   6, 0, 12, 65520
    cmpli   7, 0, 12, 65532
    ldu     12, 8(6)
    cmpli   0, 0, 12, 3
    cmpli   1, 0, 12, 15
    cmpli   2, 0, 12, 255
    cmpli   3, 0, 12, 32767
    cmpli   4, 0, 12, 32768
    cmpli   5, 0, 12, 65280
    cmpli   6, 0, 12, 65520
    cmpli   7, 0, 12, 65532
    ldu     12, 8(6)
    cmpli   0, 0, 12, 3
    cmpli   1, 0, 12, 15
    cmpli   2, 0, 12, 255
    cmpli   3, 0, 12, 32767
    cmpli   4, 0, 12, 32768
    cmpli   5, 0, 12, 65280
    cmpli   6, 0, 12, 65520
    cmpli   7, 0, 12, 65532
    ldu     12, 8(6)
    cmpli   0, 0, 12, 3
    cmpli   1, 0, 12, 15
    cmpli   2, 0, 12, 255
    cmpli   3, 0, 12, 32767
    cmpli   4, 0, 12, 32768
    cmpli   5, 0, 12, 65280
    cmpli   6, 0, 12, 65520
    cmpli   7, 0, 12, 65532
    ldu     12, 8(6)
    cmpli   0, 0, 12, 3
    cmpli   1, 0, 12, 15
    cmpli   2, 0, 12, 255
    cmpli   3, 0, 12, 32767
    cmpli   4, 0, 12, 32768
    cmpli   5, 0, 12, 65280
    cmpli   6, 0, 12, 65520
    cmpli   7, 0, 12, 65532
    ldu     12, 8(6)
    cmpli   0, 0, 12, 3
    cmpli   1, 0, 12, 15
    cmpli   2, 0, 12, 255
    cmpli   3, 0, 12, 32767
    cmpli   4, 0, 12, 32768
    cmpli   5, 0, 12, 65280
    cmpli   6, 0, 12, 65520
    cmpli   7, 0, 12, 65532
    ldu     12, 8(6)
    cmpli   0, 0, 12, 3
    cmpli   1, 0, 12, 15
    cmpli   2, 0, 12, 255
    cmpli   3, 0, 12, 32767
    cmpli   4, 0, 12, 32768
    cmpli   5, 0, 12, 65280
    cmpli   6, 0, 12, 65520
    cmpli   7, 0, 12, 65532
    ldu     12, 8(6)
    cmpli   0, 0, 12, 3
    cmpli   1, 0, 12, 15
    cmpli   2, 0, 12, 255
    cmpli   3, 0, 12, 32767
    cmpli   4, 0, 12, 32768
    cmpli   5, 0, 12, 65280
    cmpli   6, 0, 12, 65520
    cmpli   7, 0, 12, 65532
    ldu     12, 8(6)
    cmpli   0, 0, 12, 3
    cmpli   1, 0, 12, 15
    cmpli   2, 0, 12, 255
    cmpli   3, 0, 12, 32767
    cmpli   4, 0, 12, 32768
    cmpli   5, 0, 12, 65280
    cmpli   6, 0, 12, 65520
    cmpli   7, 0, 12, 65532
    ldu     12, 8(6)
    cmpli   0, 0, 12, 3
    cmpli   1, 0, 12, 15
    cmpli   2, 0, 12, 255
    cmpli   3, 0, 12, 32767
    cmpli   4, 0, 12, 32768
    cmpli   5, 0, 12, 65280
    cmpli   6, 0, 12, 65520
    cmpli   7, 0, 12, 65532
    ldu     12, 8(6)
    cmpli   0, 0, 12, 3
    cmpli   1, 0, 12, 15
    cmpli   2, 0, 12, 255
    cmpli   3, 0, 12, 32767
    cmpli   4, 0, 12, 32768
    cmpli   5, 0, 12, 65280
    cmpli   6, 0, 12, 65520
    cmpli   7, 0, 12, 65532
    ldu     12, 8(6)
    cmpli   0, 0, 12, 3
    cmpli   1, 0, 12, 15
    cmpli   2, 0, 12, 255
    cmpli   3, 0, 12, 32767
    cmpli   4, 0, 12, 32768
    cmpli   5, 0, 12, 65280
    cmpli   6, 0, 12, 65520
    cmpli   7, 0, 12, 65532
    ldu     12, 8(6)
    cmpli   0, 0, 12, 3
    cmpli   1, 0, 12, 15
    cmpli   2, 0, 12, 255
    cmpli   3, 0, 12, 32767
    cmpli   4, 0, 12, 32768
    cmpli   5, 0, 12, 65280
    cmpli   6, 0, 12, 65520
    cmpli   7, 0, 12, 65532
    ldu     12, 8(6)
    cmpli   0, 0, 12, 3
    cmpli   1, 0, 12, 15
    cmpli   2, 0, 12, 255
    cmpli   3, 0, 12, 32767
    cmpli   4, 0, 12, 32768
    cmpli   5, 0, 12, 65280
    cmpli   6, 0, 12, 65520
    cmpli   7, 0, 12, 65532
    ldu     12, 8(6)
    cmpli   0, 0, 12, 3
    cmpli   1, 0, 12, 15
    cmpli   2, 0, 12, 255
    cmpli   3, 0, 12, 32767
    cmpli   4, 0, 12, 32768
    cmpli   5, 0, 12, 65280
    cmpli   6, 0, 12, 65520
    cmpli   7, 0, 12, 65532

    # When L = 1
    mr      6, 5
    ld      12, 0(6)
    cmpli   0, 1, 12, 3
    cmpli   1, 1, 12, 15
    cmpli   2, 1, 12, 255
    cmpli   3, 1, 12, 32767
    cmpli   4, 1, 12, 32768
    cmpli   5, 1, 12, 65280
    cmpli   6, 1, 12, 65520
    cmpli   7, 1, 12, 65532
    ldu     12, 8(6)
    cmpli   0, 1, 12, 3
    cmpli   1, 1, 12, 15
    cmpli   2, 1, 12, 255
    cmpli   3, 1, 12, 32767
    cmpli   4, 1, 12, 32768
    cmpli   5, 1, 12, 65280
    cmpli   6, 1, 12, 65520
    cmpli   7, 1, 12, 65532
    ldu     12, 8(6)
    cmpli   0, 1, 12, 3
    cmpli   1, 1, 12, 15
    cmpli   2, 1, 12, 255
    cmpli   3, 1, 12, 32767
    cmpli   4, 1, 12, 32768
    cmpli   5, 1, 12, 65280
    cmpli   6, 1, 12, 65520
    cmpli   7, 1, 12, 65532
    ldu     12, 8(6)
    cmpli   0, 1, 12, 3
    cmpli   1, 1, 12, 15
    cmpli   2, 1, 12, 255
    cmpli   3, 1, 12, 32767
    cmpli   4, 1, 12, 32768
    cmpli   5, 1, 12, 65280
    cmpli   6, 1, 12, 65520
    cmpli   7, 1, 12, 65532
    ldu     12, 8(6)
    cmpli   0, 1, 12, 3
    cmpli   1, 1, 12, 15
    cmpli   2, 1, 12, 255
    cmpli   3, 1, 12, 32767
    cmpli   4, 1, 12, 32768
    cmpli   5, 1, 12, 65280
    cmpli   6, 1, 12, 65520
    cmpli   7, 1, 12, 65532
    ldu     12, 8(6)
    cmpli   0, 1, 12, 3
    cmpli   1, 1, 12, 15
    cmpli   2, 1, 12, 255
    cmpli   3, 1, 12, 32767
    cmpli   4, 1, 12, 32768
    cmpli   5, 1, 12, 65280
    cmpli   6, 1, 12, 65520
    cmpli   7, 1, 12, 65532
    ldu     12, 8(6)
    cmpli   0, 1, 12, 3
    cmpli   1, 1, 12, 15
    cmpli   2, 1, 12, 255
    cmpli   3, 1, 12, 32767
    cmpli   4, 1, 12, 32768
    cmpli   5, 1, 12, 65280
    cmpli   6, 1, 12, 65520
    cmpli   7, 1, 12, 65532
    ldu     12, 8(6)
    cmpli   0, 1, 12, 3
    cmpli   1, 1, 12, 15
    cmpli   2, 1, 12, 255
    cmpli   3, 1, 12, 32767
    cmpli   4, 1, 12, 32768
    cmpli   5, 1, 12, 65280
    cmpli   6, 1, 12, 65520
    cmpli   7, 1, 12, 65532
    ldu     12, 8(6)
    cmpli   0, 1, 12, 3
    cmpli   1, 1, 12, 15
    cmpli   2, 1, 12, 255
    cmpli   3, 1, 12, 32767
    cmpli   4, 1, 12, 32768
    cmpli   5, 1, 12, 65280
    cmpli   6, 1, 12, 65520
    cmpli   7, 1, 12, 65532
    ldu     12, 8(6)
    cmpli   0, 1, 12, 3
    cmpli   1, 1, 12, 15
    cmpli   2, 1, 12, 255
    cmpli   3, 1, 12, 32767
    cmpli   4, 1, 12, 32768
    cmpli   5, 1, 12, 65280
    cmpli   6, 1, 12, 65520
    cmpli   7, 1, 12, 65532
    ldu     12, 8(6)
    cmpli   0, 1, 12, 3
    cmpli   1, 1, 12, 15
    cmpli   2, 1, 12, 255
    cmpli   3, 1, 12, 32767
    cmpli   4, 1, 12, 32768
    cmpli   5, 1, 12, 65280
    cmpli   6, 1, 12, 65520
    cmpli   7, 1, 12, 65532
    ldu     12, 8(6)
    cmpli   0, 1, 12, 3
    cmpli   1, 1, 12, 15
    cmpli   2, 1, 12, 255
    cmpli   3, 1, 12, 32767
    cmpli   4, 1, 12, 32768
    cmpli   5, 1, 12, 65280
    cmpli   6, 1, 12, 65520
    cmpli   7, 1, 12, 65532
    ldu     12, 8(6)
    cmpli   0, 1, 12, 3
    cmpli   1, 1, 12, 15
    cmpli   2, 1, 12, 255
    cmpli   3, 1, 12, 32767
    cmpli   4, 1, 12, 32768
    cmpli   5, 1, 12, 65280
    cmpli   6, 1, 12, 65520
    cmpli   7, 1, 12, 65532
    ldu     12, 8(6)
    cmpli   0, 1, 12, 3
    cmpli   1, 1, 12, 15
    cmpli   2, 1, 12, 255
    cmpli   3, 1, 12, 32767
    cmpli   4, 1, 12, 32768
    cmpli   5, 1, 12, 65280
    cmpli   6, 1, 12, 65520
    cmpli   7, 1, 12, 65532
    ldu     12, 8(6)
    cmpli   0, 1, 12, 3
    cmpli   1, 1, 12, 15
    cmpli   2, 1, 12, 255
    cmpli   3, 1, 12, 32767
    cmpli   4, 1, 12, 32768
    cmpli   5, 1, 12, 65280
    cmpli   6, 1, 12, 65520
    cmpli   7, 1, 12, 65532
    ldu     12, 8(6)
    cmpli   0, 1, 12, 3
    cmpli   1, 1, 12, 15
    cmpli   2, 1, 12, 255
    cmpli   3, 1, 12, 32767
    cmpli   4, 1, 12, 32768
    cmpli   5, 1, 12, 65280
    cmpli   6, 1, 12, 65520
    cmpli   7, 1, 12, 65532

    # Destroy stack frame
    addi    1, 1, 16
    mtlr    0

    # Return
    blr

.data
.align 3
test_data:
    .quad 0x0000000000000003
    .quad 0x00000000fffffffc
    .quad 0x00000000000000ff
    .quad 0x00000000fffffff0
    .quad 0x00000000000000ff
    .quad 0x00000000ffffff00
    .quad 0x0000000000007fff
    .quad 0x00000000ffff8000
    .quad 0xffffffff00000003
    .quad 0xfffffffffffffffc
    .quad 0xffffffff000000ff
    .quad 0xfffffffffffffff0
    .quad 0xffffffff000000ff
    .quad 0xffffffffffffff00
    .quad 0xffffffff00007fff
    .quad 0xffffffffffff8000
