.machine    power8
.abiversion 2

.text
.global     _start

_start:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Run test cases
    bl      run_test

    # Call exit()
    li      0, 1  # Syscall number (sys_exit)
    li      3, 1  # First argument is exit code
    sc            # Call kernel

run_test:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Do some work
    lis     5, test_data@h
    ori     5, 5, test_data@l
    addi    6, 5, 8

    ld      12, 0(5)
    ld      13, 0(6)
    bpermd  14, 12, 13
    ldu     12, 16(5)
    ldu     13, 16(6)
    bpermd  14, 12, 13
    ldu     12, 16(5)
    ldu     13, 16(6)
    bpermd  14, 12, 13
    ldu     12, 16(5)
    ldu     13, 16(6)
    bpermd  14, 12, 13
    ldu     12, 16(5)
    ldu     13, 16(6)
    bpermd  14, 12, 13
    ldu     12, 16(5)
    ldu     13, 16(6)
    bpermd  14, 12, 13
    ldu     12, 16(5)
    ldu     13, 16(6)
    bpermd  14, 12, 13
    ldu     12, 16(5)
    ldu     13, 16(6)
    bpermd  14, 12, 13
    ldu     12, 16(5)
    ldu     13, 16(6)
    bpermd  14, 12, 13
    ldu     12, 16(5)
    ldu     13, 16(6)
    bpermd  14, 12, 13
    ldu     12, 16(5)
    ldu     13, 16(6)
    bpermd  14, 12, 13
    ldu     12, 16(5)
    ldu     13, 16(6)
    bpermd  14, 12, 13
    ldu     12, 16(5)
    ldu     13, 16(6)
    bpermd  14, 12, 13
    ldu     12, 16(5)
    ldu     13, 16(6)
    bpermd  14, 12, 13
    ldu     12, 16(5)
    ldu     13, 16(6)
    bpermd  14, 12, 13
    ldu     12, 16(5)
    ldu     13, 16(6)
    bpermd  14, 12, 13
    ldu     12, 16(5)
    ldu     13, 16(6)
    bpermd  14, 12, 13
    ldu     12, 16(5)
    ldu     13, 16(6)
    bpermd  14, 12, 13
    ldu     12, 16(5)
    ldu     13, 16(6)
    bpermd  14, 12, 13
    ldu     12, 16(5)
    ldu     13, 16(6)
    bpermd  14, 12, 13
    ldu     12, 16(5)
    ldu     13, 16(6)
    bpermd  14, 12, 13
    ldu     12, 16(5)
    ldu     13, 16(6)
    bpermd  14, 12, 13
    ldu     12, 16(5)
    ldu     13, 16(6)
    bpermd  14, 12, 13
    ldu     12, 16(5)
    ldu     13, 16(6)
    bpermd  14, 12, 13
    ldu     12, 16(5)
    ldu     13, 16(6)
    bpermd  14, 12, 13

    # Destroy stack frame
    addi    1, 1, 16
    mtlr    0

    # Return
    blr

.data
.align 3
test_data:
    .quad 0x000f000f000f000f, 0x000f000f000f000f
    .quad 0x000f000f000f000f, 0x0f0f0f0f0f0f0f0f
    .quad 0x000f000f000f000f, 0xf0000000f0000000
    .quad 0x000f000f000f000f, 0xf000f000f000f000
    .quad 0x000f000f000f000f, 0xf0f0f0f0f0f0f0f0
    .quad 0x0f0f0f0f0f0f0f0f, 0x000f000f000f000f
    .quad 0x0f0f0f0f0f0f0f0f, 0x0f0f0f0f0f0f0f0f
    .quad 0x0f0f0f0f0f0f0f0f, 0xf0000000f0000000
    .quad 0x0f0f0f0f0f0f0f0f, 0xf000f000f000f000
    .quad 0x0f0f0f0f0f0f0f0f, 0xf0f0f0f0f0f0f0f0
    .quad 0xf0000000f0000000, 0x000f000f000f000f
    .quad 0xf0000000f0000000, 0x0f0f0f0f0f0f0f0f
    .quad 0xf0000000f0000000, 0xf0000000f0000000
    .quad 0xf0000000f0000000, 0xf000f000f000f000
    .quad 0xf0000000f0000000, 0xf0f0f0f0f0f0f0f0
    .quad 0xf000f000f000f000, 0x000f000f000f000f
    .quad 0xf000f000f000f000, 0x0f0f0f0f0f0f0f0f
    .quad 0xf000f000f000f000, 0xf0000000f0000000
    .quad 0xf000f000f000f000, 0xf000f000f000f000
    .quad 0xf000f000f000f000, 0xf0f0f0f0f0f0f0f0
    .quad 0xf0f0f0f0f0f0f0f0, 0x000f000f000f000f
    .quad 0xf0f0f0f0f0f0f0f0, 0x0f0f0f0f0f0f0f0f
    .quad 0xf0f0f0f0f0f0f0f0, 0xf0000000f0000000
    .quad 0xf0f0f0f0f0f0f0f0, 0xf000f000f000f000
    .quad 0xf0f0f0f0f0f0f0f0, 0xf0f0f0f0f0f0f0f0
