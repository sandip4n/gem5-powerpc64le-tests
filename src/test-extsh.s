.machine    power8
.abiversion 2

.text
.global     _start

_start:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Run test cases
    bl      run_test

    # Call exit()
    li      0, 1  # Syscall number (sys_exit)
    li      3, 1  # First argument is exit code
    sc            # Call kernel

run_test:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Do some work
    lis     5, test_data@h
    ori     5, 5, test_data@l

    mr      6, 5
    ld      12, 0(6)
    extsh   13, 12
    ldu     12, 8(6)
    extsh   13, 12
    ldu     12, 8(6)
    extsh   13, 12
    ldu     12, 8(6)
    extsh   13, 12
    ldu     12, 8(6)
    extsh   13, 12
    ldu     12, 8(6)
    extsh   13, 12
    ldu     12, 8(6)
    extsh   13, 12
    ldu     12, 8(6)
    extsh   13, 12
    ldu     12, 8(6)
    extsh   13, 12
    ldu     12, 8(6)
    extsh   13, 12
    ldu     12, 8(6)
    extsh   13, 12
    ldu     12, 8(6)
    extsh   13, 12
    ldu     12, 8(6)
    extsh   13, 12
    ldu     12, 8(6)
    extsh   13, 12
    ldu     12, 8(6)
    extsh   13, 12

    mr      6, 5
    ld      12, 0(6)
    extsh.  13, 12
    ldu     12, 8(6)
    extsh.  13, 12
    ldu     12, 8(6)
    extsh.  13, 12
    ldu     12, 8(6)
    extsh.  13, 12
    ldu     12, 8(6)
    extsh.  13, 12
    ldu     12, 8(6)
    extsh.  13, 12
    ldu     12, 8(6)
    extsh.  13, 12
    ldu     12, 8(6)
    extsh.  13, 12
    ldu     12, 8(6)
    extsh.  13, 12
    ldu     12, 8(6)
    extsh.  13, 12
    ldu     12, 8(6)
    extsh.  13, 12
    ldu     12, 8(6)
    extsh.  13, 12
    ldu     12, 8(6)
    extsh.  13, 12
    ldu     12, 8(6)
    extsh.  13, 12
    ldu     12, 8(6)
    extsh.  13, 12

    # Destroy stack frame
    addi    1, 1, 16
    mtlr    0

    # Return
    blr

.data
.align 3
test_data:
    .quad 0x0000000000000000
    .quad 0x0000000000001000
    .quad 0x0000000000002000
    .quad 0x0000000000004000
    .quad 0x0000000000008000
    .quad 0x00000000ffff0000
    .quad 0x00000000ffff1000
    .quad 0x00000000ffff2000
    .quad 0x00000000ffff4000
    .quad 0x00000000ffff8000
    .quad 0xffffffffffff0000
    .quad 0xffffffffffff1000
    .quad 0xffffffffffff2000
    .quad 0xffffffffffff4000
    .quad 0xffffffffffff8000
