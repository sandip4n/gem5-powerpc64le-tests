.machine    power8
.abiversion 2

.text
.global     _start

_start:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Run test cases
    bl      run_test

    # Call exit()
    li      0, 1  # Syscall number (sys_exit)
    li      3, 1  # First argument is exit code
    sc            # Call kernel

run_test:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Do some work
    addi    5, 0, 0
    addi    12, 5, 4
    addi    12, 5, -4

    addi    5, 0, 4
    addi    12, 5, 4
    addi    12, 5, -4

    addi    5, 0, -4
    addi    12, 5, 4
    addi    12, 5, -4

    # Destroy stack frame
    addi    1, 1, 16
    mtlr    0

    # Return
    blr
