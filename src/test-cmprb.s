.machine    power8
.abiversion 2

.text
.global     _start

_start:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Run test cases
    bl      run_test

    # Call exit()
    li      0, 1  # Syscall number (sys_exit)
    li      3, 1  # First argument is exit code
    sc            # Call kernel

run_test:
    # Setup stack frame
    mflr    0
    stw     0, 8(1)
    stwu    1, -16(1)

    # Do some work
    lis     5, test_data@h
    ori     5, 5, test_data@l

    # When L = 0
    mr      6, 5
    li      12, 0
    ld      13, 0(6)
    cmprb   0, 0, 12, 13
    ldu     13, 8(6)
    cmprb   1, 0, 12, 13
    ldu     13, 8(6)
    cmprb   2, 0, 12, 13
    ldu     13, 8(6)
    cmprb   3, 0, 12, 13
    ldu     3, 8(6)
    cmprb   4, 0, 12, 13
    ldu     13, 8(6)
    cmprb   5, 0, 12, 13
    ldu     13, 8(6)
    cmprb   6, 0, 12, 13
    ldu     13, 8(6)
    cmprb   7, 0, 12, 13
    ldu     13, 8(6)
    cmprb   0, 0, 12, 13
    ldu     13, 8(6)
    cmprb   1, 0, 12, 13
    ldu     13, 8(6)
    cmprb   2, 0, 12, 13
    ldu     13, 8(6)
    cmprb   3, 0, 12, 13
    ldu     13, 8(6)
    cmprb   4, 0, 12, 13
    ldu     13, 8(6)
    cmprb   5, 0, 12, 13
    ldu     13, 8(6)
    cmprb   6, 0, 12, 13
    ldu     13, 8(6)
    cmprb   7, 0, 12, 13
    mr      6, 5
    li      12, 53
    ld      13, 0(6)
    cmprb   0, 0, 12, 13
    ldu     13, 8(6)
    cmprb   1, 0, 12, 13
    ldu     13, 8(6)
    cmprb   2, 0, 12, 13
    ldu     13, 8(6)
    cmprb   3, 0, 12, 13
    ldu     3, 8(6)
    cmprb   4, 0, 12, 13
    ldu     13, 8(6)
    cmprb   5, 0, 12, 13
    ldu     13, 8(6)
    cmprb   6, 0, 12, 13
    ldu     13, 8(6)
    cmprb   7, 0, 12, 13
    ldu     13, 8(6)
    cmprb   0, 0, 12, 13
    ldu     13, 8(6)
    cmprb   1, 0, 12, 13
    ldu     13, 8(6)
    cmprb   2, 0, 12, 13
    ldu     13, 8(6)
    cmprb   3, 0, 12, 13
    ldu     13, 8(6)
    cmprb   4, 0, 12, 13
    ldu     13, 8(6)
    cmprb   5, 0, 12, 13
    ldu     13, 8(6)
    cmprb   6, 0, 12, 13
    ldu     13, 8(6)
    cmprb   7, 0, 12, 13
    mr      6, 5
    li      12, 78
    ld      13, 0(6)
    cmprb   0, 0, 12, 13
    ldu     13, 8(6)
    cmprb   1, 0, 12, 13
    ldu     13, 8(6)
    cmprb   2, 0, 12, 13
    ldu     13, 8(6)
    cmprb   3, 0, 12, 13
    ldu     3, 8(6)
    cmprb   4, 0, 12, 13
    ldu     13, 8(6)
    cmprb   5, 0, 12, 13
    ldu     13, 8(6)
    cmprb   6, 0, 12, 13
    ldu     13, 8(6)
    cmprb   7, 0, 12, 13
    ldu     13, 8(6)
    cmprb   0, 0, 12, 13
    ldu     13, 8(6)
    cmprb   1, 0, 12, 13
    ldu     13, 8(6)
    cmprb   2, 0, 12, 13
    ldu     13, 8(6)
    cmprb   3, 0, 12, 13
    ldu     13, 8(6)
    cmprb   4, 0, 12, 13
    ldu     13, 8(6)
    cmprb   5, 0, 12, 13
    ldu     13, 8(6)
    cmprb   6, 0, 12, 13
    ldu     13, 8(6)
    cmprb   7, 0, 12, 13
    mr      6, 5
    li      12, 110
    ld      13, 0(6)
    cmprb   0, 0, 12, 13
    ldu     13, 8(6)
    cmprb   1, 0, 12, 13
    ldu     13, 8(6)
    cmprb   2, 0, 12, 13
    ldu     13, 8(6)
    cmprb   3, 0, 12, 13
    ldu     3, 8(6)
    cmprb   4, 0, 12, 13
    ldu     13, 8(6)
    cmprb   5, 0, 12, 13
    ldu     13, 8(6)
    cmprb   6, 0, 12, 13
    ldu     13, 8(6)
    cmprb   7, 0, 12, 13
    ldu     13, 8(6)
    cmprb   0, 0, 12, 13
    ldu     13, 8(6)
    cmprb   1, 0, 12, 13
    ldu     13, 8(6)
    cmprb   2, 0, 12, 13
    ldu     13, 8(6)
    cmprb   3, 0, 12, 13
    ldu     13, 8(6)
    cmprb   4, 0, 12, 13
    ldu     13, 8(6)
    cmprb   5, 0, 12, 13
    ldu     13, 8(6)
    cmprb   6, 0, 12, 13
    ldu     13, 8(6)
    cmprb   7, 0, 12, 13
    mr      6, 5
    li      12, 255
    ld      13, 0(6)
    cmprb   0, 0, 12, 13
    ldu     13, 8(6)
    cmprb   1, 0, 12, 13
    ldu     13, 8(6)
    cmprb   2, 0, 12, 13
    ldu     13, 8(6)
    cmprb   3, 0, 12, 13
    ldu     3, 8(6)
    cmprb   4, 0, 12, 13
    ldu     13, 8(6)
    cmprb   5, 0, 12, 13
    ldu     13, 8(6)
    cmprb   6, 0, 12, 13
    ldu     13, 8(6)
    cmprb   7, 0, 12, 13
    ldu     13, 8(6)
    cmprb   0, 0, 12, 13
    ldu     13, 8(6)
    cmprb   1, 0, 12, 13
    ldu     13, 8(6)
    cmprb   2, 0, 12, 13
    ldu     13, 8(6)
    cmprb   3, 0, 12, 13
    ldu     13, 8(6)
    cmprb   4, 0, 12, 13
    ldu     13, 8(6)
    cmprb   5, 0, 12, 13
    ldu     13, 8(6)
    cmprb   6, 0, 12, 13
    ldu     13, 8(6)
    cmprb   7, 0, 12, 13

    # When L = 1
    mr      6, 5
    li      12, 0
    ld      13, 0(6)
    cmprb   0, 1, 12, 13
    ldu     13, 8(6)
    cmprb   1, 1, 12, 13
    ldu     13, 8(6)
    cmprb   2, 1, 12, 13
    ldu     13, 8(6)
    cmprb   3, 1, 12, 13
    ldu     3, 8(6)
    cmprb   4, 1, 12, 13
    ldu     13, 8(6)
    cmprb   5, 1, 12, 13
    ldu     13, 8(6)
    cmprb   6, 1, 12, 13
    ldu     13, 8(6)
    cmprb   7, 1, 12, 13
    ldu     13, 8(6)
    cmprb   0, 1, 12, 13
    ldu     13, 8(6)
    cmprb   1, 1, 12, 13
    ldu     13, 8(6)
    cmprb   2, 1, 12, 13
    ldu     13, 8(6)
    cmprb   3, 1, 12, 13
    ldu     13, 8(6)
    cmprb   4, 1, 12, 13
    ldu     13, 8(6)
    cmprb   5, 1, 12, 13
    ldu     13, 8(6)
    cmprb   6, 1, 12, 13
    ldu     13, 8(6)
    cmprb   7, 1, 12, 13
    mr      6, 5
    li      12, 53
    ld      13, 0(6)
    cmprb   0, 1, 12, 13
    ldu     13, 8(6)
    cmprb   1, 1, 12, 13
    ldu     13, 8(6)
    cmprb   2, 1, 12, 13
    ldu     13, 8(6)
    cmprb   3, 1, 12, 13
    ldu     3, 8(6)
    cmprb   4, 1, 12, 13
    ldu     13, 8(6)
    cmprb   5, 1, 12, 13
    ldu     13, 8(6)
    cmprb   6, 1, 12, 13
    ldu     13, 8(6)
    cmprb   7, 1, 12, 13
    ldu     13, 8(6)
    cmprb   0, 1, 12, 13
    ldu     13, 8(6)
    cmprb   1, 1, 12, 13
    ldu     13, 8(6)
    cmprb   2, 1, 12, 13
    ldu     13, 8(6)
    cmprb   3, 1, 12, 13
    ldu     13, 8(6)
    cmprb   4, 1, 12, 13
    ldu     13, 8(6)
    cmprb   5, 1, 12, 13
    ldu     13, 8(6)
    cmprb   6, 1, 12, 13
    ldu     13, 8(6)
    cmprb   7, 1, 12, 13
    mr      6, 5
    li      12, 78
    ld      13, 0(6)
    cmprb   0, 1, 12, 13
    ldu     13, 8(6)
    cmprb   1, 1, 12, 13
    ldu     13, 8(6)
    cmprb   2, 1, 12, 13
    ldu     13, 8(6)
    cmprb   3, 1, 12, 13
    ldu     3, 8(6)
    cmprb   4, 1, 12, 13
    ldu     13, 8(6)
    cmprb   5, 1, 12, 13
    ldu     13, 8(6)
    cmprb   6, 1, 12, 13
    ldu     13, 8(6)
    cmprb   7, 1, 12, 13
    ldu     13, 8(6)
    cmprb   0, 1, 12, 13
    ldu     13, 8(6)
    cmprb   1, 1, 12, 13
    ldu     13, 8(6)
    cmprb   2, 1, 12, 13
    ldu     13, 8(6)
    cmprb   3, 1, 12, 13
    ldu     13, 8(6)
    cmprb   4, 1, 12, 13
    ldu     13, 8(6)
    cmprb   5, 1, 12, 13
    ldu     13, 8(6)
    cmprb   6, 1, 12, 13
    ldu     13, 8(6)
    cmprb   7, 1, 12, 13
    mr      6, 5
    li      12, 110
    ld      13, 0(6)
    cmprb   0, 1, 12, 13
    ldu     13, 8(6)
    cmprb   1, 1, 12, 13
    ldu     13, 8(6)
    cmprb   2, 1, 12, 13
    ldu     13, 8(6)
    cmprb   3, 1, 12, 13
    ldu     3, 8(6)
    cmprb   4, 1, 12, 13
    ldu     13, 8(6)
    cmprb   5, 1, 12, 13
    ldu     13, 8(6)
    cmprb   6, 1, 12, 13
    ldu     13, 8(6)
    cmprb   7, 1, 12, 13
    ldu     13, 8(6)
    cmprb   0, 1, 12, 13
    ldu     13, 8(6)
    cmprb   1, 1, 12, 13
    ldu     13, 8(6)
    cmprb   2, 1, 12, 13
    ldu     13, 8(6)
    cmprb   3, 1, 12, 13
    ldu     13, 8(6)
    cmprb   4, 1, 12, 13
    ldu     13, 8(6)
    cmprb   5, 1, 12, 13
    ldu     13, 8(6)
    cmprb   6, 1, 12, 13
    ldu     13, 8(6)
    cmprb   7, 1, 12, 13
    mr      6, 5
    li      12, 255
    ld      13, 0(6)
    cmprb   0, 1, 12, 13
    ldu     13, 8(6)
    cmprb   1, 1, 12, 13
    ldu     13, 8(6)
    cmprb   2, 1, 12, 13
    ldu     13, 8(6)
    cmprb   3, 1, 12, 13
    ldu     3, 8(6)
    cmprb   4, 1, 12, 13
    ldu     13, 8(6)
    cmprb   5, 1, 12, 13
    ldu     13, 8(6)
    cmprb   6, 1, 12, 13
    ldu     13, 8(6)
    cmprb   7, 1, 12, 13
    ldu     13, 8(6)
    cmprb   0, 1, 12, 13
    ldu     13, 8(6)
    cmprb   1, 1, 12, 13
    ldu     13, 8(6)
    cmprb   2, 1, 12, 13
    ldu     13, 8(6)
    cmprb   3, 1, 12, 13
    ldu     13, 8(6)
    cmprb   4, 1, 12, 13
    ldu     13, 8(6)
    cmprb   5, 1, 12, 13
    ldu     13, 8(6)
    cmprb   6, 1, 12, 13
    ldu     13, 8(6)
    cmprb   7, 1, 12, 13

    # Destroy stack frame
    addi    1, 1, 16
    mtlr    0

    # Return
    blr

.data
.align 3
test_data:
    .quad 0x00000000000000ff
    .quad 0xff0000000000ff00
    .quad 0xffff000000003039
    .quad 0xffffffff00003930
    .quad 0x000000000000415A
    .quad 0xff00000000005A41
    .quad 0xffff00000000617A
    .quad 0xffffffff00007A61

    .quad 0x0000000000ff00ff
    .quad 0xff00000000ffff00
    .quad 0xffff0000ff0000ff
    .quad 0xffffffffff00ff00
    .quad 0x00000000617A415A
    .quad 0xff000000617A5A41
    .quad 0xffff00007A61415A
    .quad 0xffffffff7A615A41
